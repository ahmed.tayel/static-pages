import { Button, createStyles, Divider, IconButton, makeStyles, Theme, Typography } from '@material-ui/core';
import React from 'react'
import { default as Logo } from '../assets/logo.svg'
import { default as BriefcaseIcon } from '../assets/briefcase.svg'
import { default as Mail } from '../assets/mail.svg'
import { default as Telephone } from '../assets/telephone.svg'
import { default as Fax } from '../assets/fax.svg'
import { default as Facebook } from '../assets/facebook.svg'
import { default as Instagram } from '../assets/instagram.svg'
import { default as Twitter } from '../assets/twitter.svg'


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            minWidth: 275,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            background: 'transparent linear-gradient(180deg, #323A87 0%, #211D57 100%) 0% 0% no-repeat padding-box',
            height: 466,
            [theme.breakpoints.down('xs')]: {
                flexDirection: 'column',
                height: 1200,
            }
        },
        divider: {
            color: '#FFFFFF',
            maxHeight: 236,
            backgroundColor: '#FFFFFF',
            [theme.breakpoints.down('xs')] : {
                height:1,
                width: '60%',
                
            }
        },
        subtitle: {
            fontSize: 26,
            fontWeight: 'bold',
            textAlign: 'left',
            color: '#ffffff'
        },
        sectionLogo: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            margin: theme.spacing(5),
            flexDirection: 'column',
            maxHeight: 200,
            [theme.breakpoints.down('xs')]: {
                
            }
        },
        sectionContent: {
            display: 'flex',
            alignItems: 'left',
            justifyContent: 'flex-start',
            margin: theme.spacing(5),
            flexDirection: 'column',
            color: '#ffffff',
            [theme.breakpoints.down('xs')]: {
                
            }
        },
        sectionLinks: {
            display: 'flex',
            alignItems: 'left',
            justifyContent: 'flex-start',
            margin: theme.spacing(5),
            flexDirection: 'column',
            color: '#ffffff',
            width: 340,
            marginLeft: theme.spacing(12)
        },
        content: {
            fontSize: 21,
            textTransform: 'capitalize',
            borderRadius: 15,
            color: '#ffffff',
            justifyContent:'flex-start',
            '&:hover': {
                background: 'transparent' 
            },
            '&:active': {
                background: 'transparent'
            }
        },
        entry: {
            display: 'flex',
            alignItems: 'center',
            alignContent: 'left',
            justifyContent: 'flex-start',
            textAlign: 'left',
            margin: theme.spacing(1)
        },
        icon: {
            marginRight: theme.spacing(2)
        },
        button: {
            width: 280,
            height: 58,
            border: '2px solid #ffffff',
            color: '#ffffff',
            textTransform: 'capitalize',
            borderRadius: 15,
            fontSize: 24,

        },
        socials: {
            display: 'flex',
            justifyContent: 'center',
            margin: theme.spacing(1)
        }
    }),
);

export const Footer: React.FC = () => {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <div className={classes.sectionLogo}>
                <img src={Logo} alt="Logo" />
            </div>
            <Divider className={classes.divider} orientation={window.screen.availWidth >  300   ? 'vertical' : 'horizontal'} />
            <div className={classes.sectionContent}>
                <Typography className={classes.subtitle}>
                    Contact us
                </Typography>
                <div className={classes.entry}>
                    <div className={classes.icon}>
                        <img src={BriefcaseIcon} alt="Address" />
                    </div>
                    <Typography className={classes.content}>
                        Rx REVERSE DISTRIBUTORS, <br />INC. 9255 US Highway 1 Sebastian, FL 32958
                    </Typography>
                </div>
                <div className={classes.entry}>
                    <div className={classes.icon}>
                        <img src={Telephone} alt="Phone number" />
                    </div>
                    <Typography className={classes.content}>
                        866-388-7973
                    </Typography>
                </div>
                <div className={classes.entry}>
                    <div className={classes.icon}>
                        <img src={Fax} alt="Fax number" />
                    </div>
                    <Typography className={classes.content}>
                        772-388-1260
                    </Typography>
                </div>
                <div className={classes.entry}>
                    <div className={classes.icon}>
                        <img src={Mail} alt="Email" />
                    </div>
                    <Typography className={classes.content}>
                        Contact Email
                    </Typography>
                </div>
            </div>
            <Divider className={classes.divider} orientation={window.screen.availWidth >  300   ? 'vertical' : 'horizontal'} />
            <div className={classes.sectionLinks}>
                <Typography className={classes.subtitle}>
                    Quick Links
                </Typography>
                <Button disableRipple className={classes.content}>
                    About Us
                </Button>
                <Button disableRipple className={classes.content}>
                    Services
                </Button>
                <Button disableRipple className={classes.content}>
                    Contact Us
                </Button>
                <Button disableRipple className={classes.content}>
                    Managment
                </Button>
                <Button disableRipple className={classes.content}>
                    Compliance
                </Button>

            </div>
            <Divider className={classes.divider} orientation={window.screen.availWidth >  300   ? 'vertical' : 'horizontal'} />
            <div className={classes.sectionContent}>
                <Button variant="outlined" className={classes.button}>
                    Start Return
                </Button>
                <div className={classes.socials}>
                    <IconButton >
                        <img src={Twitter} alt="Twitter social" />
                    </IconButton>
                    <IconButton >
                        <img src={Facebook} alt="Facebook social" />
                    </IconButton>
                    <IconButton >
                        <img src={Instagram} alt="Instagram social" />
                    </IconButton>
                </div>
            </div>
        </div>
    );
}