import { makeStyles } from '@material-ui/core/styles';
import { Icon } from '@material-ui/core';

const useStyles = makeStyles({
    imageIcon: {
      height: '100%'
    },
    iconRoot: {
      textAlign: 'center'
    }
  });



export default function SvgIconsSize() {
    const classes = useStyles();

    return (
        <Icon classes={{ root: classes.iconRoot }}>
            <img className={classes.imageIcon} src="/final-02.svg" />
        </Icon>
    );
}