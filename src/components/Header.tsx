import { AppBar, Button, Container, createStyles, Link, makeStyles, SvgIcon, Theme, Toolbar, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { default as LoginIcon } from '../assets/loginIcon.svg';
import { default as UserIcon } from '../assets/userIcon.svg';
import { default as Banner } from "../assets/banner.png";
import { default as Headphone } from "../assets/headphone.svg";
import { default as LogoIcon } from "../assets/logoSmall.svg";
import { HeaderButton } from '../atoms/Header Button/HeaderButton';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            height: 65,
            paddingBottom: 766,
            zIndex: 10
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            display: 'flex',
            alignItems: 'center',
            color: '#FFFFFF',
            fontSize: 19,
            textTransform: 'capitalize',
            borderRadius: 0
        },
        headphoneIcon: {
            paddingTop: 7
        },
        navbar: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',

        },
        navbarContent: {
            display: 'flex',
            justifyContent: 'space-between',
            width: 340,

        },
        navbarMain: {
            paddingLeft: 24,
            color: '#ffffff',
            [theme.breakpoints.down('xs')]: {
                paddingLeft: 0
            }
        },
        loginButton: {
            background: 'transparent linear-gradient(107deg, #A61C14 0%, #530E0A 100%) 0% 0% no-repeat padding-box',
            height: 65,
            borderRadius: 0,
            width: 188,
            [theme.breakpoints.down('xs')]: {

            }
        },
        containerPicture: {
            background: `transparent url(${Banner}) 0% 0% no-repeat padding-box`,
            height: 866,
            position: 'relative'
        },
        containerPictureHue: {
            background: '#211D57 0% 0% no-repeat padding-box',
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            mixBlendMode: 'multiply',
            opacity: 0.5
        },
        menu: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: 100,
        },
        mainContent: {
            width: '100%',
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column'
        },
        mainContentText: {
            fontSize: 65,
            fontWeight: 'bold',
            color: '#ffffff',
            textShadow: '0px 3px 6px #000000DE',
            zIndex: 1,
            [theme.breakpoints.down('xs')]: {
                fontSize: 30,
                textAlign: 'center'
            }
        },
        mainContentButton: {
            border: '3px solid #FFFFFF',
            borderRadius: '15px',
            width: 400,
            height: 91,
            color: '#ffffff',
            fontSize: 32,
            fontWeight: 'bold',
            textTransform: 'capitalize',
            [theme.breakpoints.down('xs')]: {
                marginTop: theme.spacing(3),
                width: 300,
                height: 70
            }
        },
        menuContent: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        menuItems: {
            color: '#ffffff',
            textTransform: 'capitalize',
            fontSize: 26,
            padding: theme.spacing(2),
            zIndex: 1,
            '&:active': {
                borderBottom: '3px sold blue',
                borderRadius: 3,
                '&::after': {
                    borderBottom: '3px sold blue',
                    borderRadius: 3,
                }
            },

        },
        logoIcon: {
            zIndex: 1
        }
    }),
);


export const Header: React.FC = () => {
    const classes = useStyles();
    const [windowSize, setWindowSize] = useState<boolean>(window.screen.availWidth > 500);
    useEffect(() => {
        const handleWindowResize = () => setWindowSize(window.innerWidth > 500 ? true : false);
        window.addEventListener("resize", handleWindowResize);


        return () => window.removeEventListener("resize", handleWindowResize);
    }, []);
    return (
        <div className={classes.root}>
            <AppBar position="static" style={{ backgroundColor: '#211d57' }}>
                <Toolbar disableGutters className={classes.navbar}>
                    <div className={classes.navbarMain}>
                        
                        <HeaderButton background={''} textColor={"white"} fontSize={'19'} icon={'Headphone'} label={windowSize ? 'Contact us' : ''} width={''} />
                            
                       
                    </div>
                    <div className={classes.navbarContent}>
                        <Button disableRipple startIcon={<img src={UserIcon} alt="Register icon" />} className={classes.title} color="inherit">Register</Button>
                        <Button disableRipple disableElevation startIcon={<img src={LoginIcon} alt="Login icon" />} className={classes.loginButton} color="inherit">Login</Button>
                    </div>
                </Toolbar>
            </AppBar>
            <Container className={classes.containerPicture} maxWidth='xl'>
                <div className={classes.containerPictureHue}></div>
                {windowSize ? (<div className={classes.menu}>
                    <div className={classes.logoIcon}>
                        <img src={LogoIcon} alt="" />
                    </div>
                    <div className={classes.menuContent}>
                        <Link underline="always" href="#" className={classes.menuItems}>Home</Link>
                        <Link underline="hover" href="#" className={classes.menuItems}>Services</Link>
                        <Link underline="hover" href="#" className={classes.menuItems}>Compliance</Link>
                        <Link underline="hover" href="#" className={classes.menuItems}>About Us</Link>
                    </div>
                </div>) : (<div></div>)}
                <div className={classes.mainContent}>
                    <Typography className={classes.mainContentText}>
                        We do all the work. You get the Credit!
                    </Typography>
                    <Button disableRipple variant="text" className={classes.mainContentButton}>
                        Start Return
                    </Button>
                </div>

            </Container>
        </div>
    );
}