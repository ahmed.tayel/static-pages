import { TableCellAtomProps } from "../atoms/Table Cell/TableCellAtom";
import { TableRowProps } from "../molecules/Table Row/TableRowAtom";




const headerCells: Array<TableCellAtomProps> = [];

export const createHeaderRow = (headerTitles: string[]) => {

    headerTitles.map((item:string) => {
        let headCell = {
            bold: true,
            clickable: false,
            header: true,
            onClick: undefined,
            text: item
        }
        headerCells.push(headCell)

    })
    return headerCells;
}





