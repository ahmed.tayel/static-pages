import { Accordion, AccordionDetails, AccordionSummary, Card, CardContent, Container, Typography } from '@material-ui/core';
import React from 'react'
import { TextBox } from '../../atoms/Text Box/TextBox';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useStyles } from './FaqPageStyles';

export interface FaqPageProps {
    accordians: Array<{ summary: string, details: string }>
}

export const FaqPage: React.FC<FaqPageProps> = (props: FaqPageProps) => {
    const { accordians } = props

    const classes = useStyles(props)

    const loadAccordian = () => {
        return accordians.map((accordian) => {
            return (
                <Accordion elevation={0}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.accordianSummary}>
                            {accordian.summary}
                        </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className={classes.acccordianDetails}>
                            {accordian.details}
                        </Typography>
                    </AccordionDetails>
                </Accordion>
            )
        })
    }

    return (
        <Container>
            <Card>
                <CardContent>
                    <TextBox bodyFontSize={18} headerFontSize={32} image={''} paragraphs={['']} title={"Frequently Asked Questions"} titleColor={"#A61C14"} />
                    {loadAccordian()}
                </CardContent>
            </Card>
        </Container>
    );
}