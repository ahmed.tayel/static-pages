import { ComponentStory } from '@storybook/react';
import { FaqPage } from './FaqPage';
export default {
    title: 'Pages/Faq',
    component: FaqPage,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof FaqPage> = args =>  <FaqPage {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    accordians: [
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "1- Q. What do I do if my labels didn't print?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "2- Q. How can I print more labels?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "3- Q. How do I reschedule or cancel a FedEx pick up?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "4- Q. Can I submit the return without scheduling a pick up?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "5- Q. Is there a cost to print a check?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "6- Q. Does this check expire?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "7- Q. Does the check link expire once printed?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "8- Q. What email address will my checks come from?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "9- Q. What are Deposit Services?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "10- Q. Are Deposit Services available for all checks?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "11- Q. How will I know if a Deposit Service is available?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "12- Q. When will my funds be available?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "13- Q. What does guaranteed funds mean?"
        },
        {
            details: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.',
            summary: "14- Q. Is there a cost to deposit my funds from RXRD?"
        },

    ]
}