import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface FaqPageStylesProps {

}
export const useStyles = makeStyles<Theme, FaqPageStylesProps>((theme: Theme) =>
    createStyles({
        acccordianDetails: {
            fontSize: 18,
            color: '#403F3F',
            paddingTop: '10',
            paddingBottom: '10',
            [theme.breakpoints.down('xs')]: {
                fontSize: 12
            }

        },
        accordianSummary: {
            fontSize: 18,
            color: '#403F3F',
            fontWeight: 'bold',
            marginTop: 10,
            [theme.breakpoints.down('xs')]: {
                fontSize: 12
            }
        }
    }),
);