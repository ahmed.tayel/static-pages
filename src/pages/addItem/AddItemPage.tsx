import { Container, Card } from '@material-ui/core'
import CardContent from '@material-ui/core/CardContent/CardContent'
import React from 'react'
import { CallToActionButton } from '../../atoms/callToActionButton/CallToActionButton'
import { TableOrganism } from '../../molecules/Table/TableOrganism'
import { createHeaderRow } from '../../utilities/CreateHeaderRow'
import { useStyles } from './AddItemPageStyles'

export interface AddItemPageProps {

}

export const AddItemPage: React.FC<AddItemPageProps> = (props: AddItemPageProps) => {

    const { } = props

    const classes = useStyles(props)

    const headerTitles = [ 'NDC', 'Description', 'Manufacturer', "Strength", "Dosage", "Pkg Size", "Full Quantity", "Partial Quantity"]
    return (
        <div>
            <Container>
                <Card>
                    <CardContent>
                        <div className="form"></div>
                        <div className={classes.buttonWrapper}>
                            <CallToActionButton height={"90px"} text="Add Item" width={"897px"} />
                        </div>
                        <TableOrganism header={{cells: createHeaderRow(headerTitles)}} rows={[]}/>
                    </CardContent>
                </Card>
            </Container>
        </div>
    );

}