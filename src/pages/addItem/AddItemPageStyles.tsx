import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface AddItemPageStylesProps {
    
}
export const useStyles = makeStyles<Theme, AddItemPageStylesProps>((theme: Theme) =>
    createStyles({
        buttonWrapper: {
            display: 'flex',
            justifyContent: 'center',
            margin: theme.spacing(2),
            width: 'auto'
        }
    }),
);