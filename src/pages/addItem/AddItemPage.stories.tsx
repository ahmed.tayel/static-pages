import { ComponentStory } from '@storybook/react';
import { AddItemPage } from './AddItemPage';
export default {
    title: 'Pages/Add Item',
    component: AddItemPage,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof AddItemPage> = args =>  <AddItemPage {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}