import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { AccountManagmentPageProps } from './AccountManagmentPage';

export interface AccountManagmentPageStylesProps {
    
}
export const useStyles = makeStyles<Theme, AccountManagmentPageProps>((theme: Theme) =>
    createStyles({
        
    }),
);