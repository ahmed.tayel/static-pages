import { ComponentStory } from '@storybook/react';
import { AccountManagmentPage } from './AccountManagmentPage';
export default {
    title: 'Pages/Account Managment',
    component: AccountManagmentPage,
    argTypes: {

    }
}

const Template: ComponentStory<typeof AccountManagmentPage> = args => <AccountManagmentPage {...args} />

export const Primary = Template.bind({})
Primary.args = {

}