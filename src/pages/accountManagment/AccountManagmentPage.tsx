import { Container, Card, CardContent } from '@material-ui/core';
import React from 'react'
import { CallToActionButton } from '../../atoms/callToActionButton/CallToActionButton';
import { TableOrganism } from '../../molecules/Table/TableOrganism';
import { createHeaderRow } from '../../utilities/CreateHeaderRow';
import { useStyles } from '../landing page/LandingPageStyles';

export interface AccountManagmentPageProps {

}

export const AccountManagmentPage: React.FC<AccountManagmentPageProps> = (props) => {

    const { } = props

    const classes = useStyles(props)

    const headerTitles = ['Account', "Returns", "Cheques", "Start Return"]
    
    return (
        <Container>
            <Card>
                <CardContent>
                    <div className={classes.buttonWrapper}>
                        <CallToActionButton height={"90px"} text="Create Account" width={"897px"} />
                    </div>
                    <TableOrganism 
                    header={{
                        cells: createHeaderRow(headerTitles)
                    }}

                        rows={[
                            {
                                cells: [
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "Pharmacy Name"
                                    },
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "10"
                                    },
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "20"
                                    },
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "Start New Return"
                                    }
                                ],
                            },
                            {
                                cells: [
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "Pharmacy Name"
                                    },
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "10"
                                    },
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "20"
                                    },
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "Start New Return"
                                    }
                                ],
                            },
                            {
                                cells: [
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "Pharmacy Name"
                                    },
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "10"
                                    },
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "20"
                                    },
                                    {
                                        bold: false,
                                        clickable: true,
                                        header: false,
                                        onClick: () => { console.log('You pressed me!'); },
                                        text: "Start New Return"
                                    }
                                ],
                            }
                        ]} />

                </CardContent>
            </Card>
        </Container>
    )
}