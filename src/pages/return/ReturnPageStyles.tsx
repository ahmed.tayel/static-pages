import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface ReturnPageStylesProps {
    
}
export const useStyles = makeStyles<Theme, ReturnPageStylesProps>((theme: Theme) =>
    createStyles({
        
    }),
);