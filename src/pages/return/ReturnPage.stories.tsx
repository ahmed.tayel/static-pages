import { ComponentStory } from '@storybook/react';
import { ReturnPage } from './ReturnPage';
export default {
    title: 'Pages/Return Page',
    component: ReturnPage,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof ReturnPage> = args =>  <ReturnPage {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}