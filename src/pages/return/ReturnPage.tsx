import { Container, Card, CardContent } from '@material-ui/core';
import React from 'react'
import { TableOrganism } from '../../molecules/Table/TableOrganism';
import { createHeaderRow } from '../../utilities/CreateHeaderRow';


export interface ReturnPageProps {

}

export const ReturnPage: React.FC<ReturnPageProps> = (props: ReturnPageProps) => {
   
    const headerTitles = ["Number", "Type", "User Items", "Verified Items", "Status", "UPS Label", "Reports", "ERV", "Wholesaler Credit",
    "Cheques", "Disbursemenets", "Service Fee %", "    "]
    return (
        <Container>
            <Card>
                <CardContent>
                    <TableOrganism header={{cells:createHeaderRow(headerTitles)}} rows={[]} />
                </CardContent>
            </Card>
        </Container>
    );
}