import { Card, CardContent, Container } from '@material-ui/core';
import React from 'react'
import { TableOrganism } from '../../molecules/Table/TableOrganism';
import { createHeaderRow } from '../../utilities/CreateHeaderRow';

export interface ChequePageProps {

}

export const ChequePage: React.FC<ChequePageProps> = (props: ChequePageProps) => {

    const headerTitles = ["Statements", "Payment Date", 'Invoice date', 'Cheque Amount', "Amount", "Cheques Status", "Return Number", "Cheque Number"]

    return (
        <Container>
            <Card>
                <CardContent>
                    <TableOrganism header={
                        {
                            cells: createHeaderRow(headerTitles)
                        }
                    }
                        rows={
                            [
                                {
                                    cells: [
                                        {
                                            bold: true,
                                            clickable: true,
                                            header: false,
                                            onClick: () => { console.log("You pressed me!") },
                                            text: '25847'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '14/8/2021'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '5/9/2021'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '24.589'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '30.589'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: 'Cleared'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '58962005414'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '589620'
                                        },
                                    ]
                                },
                                {
                                    cells: [
                                        {
                                            bold: true,
                                            clickable: true,
                                            header: false,
                                            onClick: () => { console.log("You pressed me!") },
                                            text: '25847'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '14/8/2021'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '5/9/2021'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '24.589'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '30.589'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: 'Cleared'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '58962005414'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '589620'
                                        }
                                    ]
                                },
                                {
                                    cells: [
                                        {
                                            bold: true,
                                            clickable: true,
                                            header: false,
                                            onClick: () => { console.log("You pressed me!") },
                                            text: '25847'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '14/8/2021'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '5/9/2021'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '24.589'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '30.589'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: 'Cleared'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '58962005414'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '589620'
                                        }
                                    ]
                                },
                                {
                                    cells: [
                                        {
                                            bold: false,
                                            clickable: true,
                                            header: false,
                                            onClick: () => { console.log("You pressed me!") },
                                            text: '25847'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '14/8/2021'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '5/9/2021'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '24.589'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '30.589'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: 'Cleared'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '58962005414'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '589620'
                                        }
                                    ]
                                },
                                {
                                    cells: [
                                        {
                                            bold: false,
                                            clickable: true,
                                            header: false,
                                            onClick: () => { console.log("You pressed me!") },
                                            text: '25847'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '14/8/2021'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '5/9/2021'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '24.589'
                                        },
                                        {
                                            bold: true,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '30.589'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: 'Cleared'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '58962005414'
                                        },
                                        {
                                            bold: false,
                                            clickable: false,
                                            header: false,
                                            onClick: undefined,
                                            text: '589620'
                                        }
                                    ]
                                }
                            ]
                        }
                    />
                </CardContent>
            </Card>
        </Container>
    );
}