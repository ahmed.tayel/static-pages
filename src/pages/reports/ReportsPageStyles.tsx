import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface ReportsPageStylesProps {
    
}
export const useStyles = makeStyles<Theme, ReportsPageStylesProps>((theme: Theme) =>
    createStyles({
        
    }),
);