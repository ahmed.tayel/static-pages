import { ComponentStory } from '@storybook/react';
import { ReportsPage } from './ReportsPage';
export default {
    title: 'Pages/Reports',
    component: ReportsPage,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof ReportsPage> = args =>  <ReportsPage {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}