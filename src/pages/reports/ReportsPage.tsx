import { Container, Card, CardContent } from '@material-ui/core';
import React from 'react'
import { TableOrganism } from '../../molecules/Table/TableOrganism';
import { createHeaderRow } from '../../utilities/CreateHeaderRow';

export interface ReportsPageProps {

}

export const ReportsPage: React.FC<ReportsPageProps> = (props: ReportsPageProps) => {
    
    
    const headerTitles = [ 'Return Number', "Invoice Date", "Service Date", "ERV","Credit Recieved", "Direct Pay", "Fees", "Amount Paid", "Last Payment Amount", "Remaining ERV", "Future Dated"]

    return (
        <Container>
            <Card>
                <CardContent>
                    <TableOrganism header={{cells:createHeaderRow(headerTitles)}} rows={[]}/>
                </CardContent>
            </Card>
        </Container>
    );
}