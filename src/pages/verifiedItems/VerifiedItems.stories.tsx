import { ComponentStory } from '@storybook/react';
import { VerifiedItems } from './VerifiedItems';
export default {
    title: 'Pages/Verified Items',
    component: VerifiedItems,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof VerifiedItems> = args =>  <VerifiedItems {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}