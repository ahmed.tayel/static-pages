import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface VerifiedItemsStylesProps {
    
}
export const useStyles = makeStyles<Theme, VerifiedItemsStylesProps>((theme: Theme) =>
    createStyles({
        
    }),
);