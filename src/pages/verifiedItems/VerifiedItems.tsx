import { Container, Card, CardContent } from '@material-ui/core';
import React from 'react'
import { TableOrganism } from '../../molecules/Table/TableOrganism';
import { createHeaderRow } from '../../utilities/CreateHeaderRow';
import { useStyles } from './VerifiedItemsStyles';

export interface VerifiedItemsProps {

}

export const VerifiedItems: React.FC<VerifiedItemsProps> = (props: VerifiedItemsProps) => {
    
    const { } = props

    const classes = useStyles(props)
    
    const headerTitles = ["NDC", "Manufacturer", "Product", "PKG Size", "Type", "Full Quantity", "Partial Quantity", "Expiration Date", "Lot Number", "Image"]

    return (
        <Container>
            <Card>
                <CardContent>
                    <TableOrganism header={{cells:createHeaderRow(headerTitles)}} rows={[]}  />
                </CardContent>
            </Card>
        </Container>
    );
}