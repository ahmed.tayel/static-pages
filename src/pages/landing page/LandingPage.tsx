import React from 'react'
import { TextBoxProps } from '../../atoms/Text Box/TextBox';
import { CardProps } from '../../molecules/Card/CardMolecule';
import { CarouselMolecule } from '../../molecules/Carousel/CarouselMolecule';
import { AboutUsOrganism } from '../../organisms/landingPage/aboutUs/AboutUsOrganism';
import { OurServicesOrganism } from '../../organisms/landingPage/ourServices/OurServicesOrganism';
import { useStyles } from './LandingPageStyles';
import { default as Group300 } from "../../static/Landing page/assets/Group 300.png";

export interface LandingPageProps {
    firstCards: Array<CardProps>,
    secondCards: Array<CardProps>,
    textBoxes: Array<TextBoxProps>
}

export const LandingPage: React.FC<LandingPageProps> = (props: LandingPageProps) => {

    const { firstCards, secondCards, textBoxes } = props

    const classes = useStyles(props)

    return (
        <div>
            <CarouselMolecule cards={firstCards} version={1} />
            <OurServicesOrganism title={'Our Services'} image={Group300} textBoxes={textBoxes} />
            <AboutUsOrganism title="About Us" image={Group300} backgroundColor={'#FBFBFB'} paragraphs={['Rx Reverse Distributors, Inc. (RxRD) is an industry leader in pharmaceutical returns processing and disposal. With a reputation for exceptional personalized service and communication, the professionals at RxRD specialize in servicing independently owned pharmacies and small to medium-sized pharmacy chains across the United States.']} />
            <CarouselMolecule cards={secondCards} version={2} />
        </div>
    );
}