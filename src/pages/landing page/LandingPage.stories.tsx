import { ComponentStory } from '@storybook/react';
import { LandingPage } from './LandingPage';
import { default as SelfService } from "../../static/Landing page/assets/SelfService.svg";
import { default as RxDisposal } from "../../static/Landing page/assets/RxDisposal.svg";
import { default as FullService } from "../../static/Landing page/assets/FullService.svg";
import { default as ExpressService } from "../../static/Landing page/assets/ExpressService.svg";
import { default as CustomerIcon } from "../../static/Landing page/assets/customer.png";

export default {
    title: 'Pages/Landing Page',
    component: LandingPage,
    argTypes: {

    }
}

const Template: ComponentStory<typeof LandingPage> = args => <LandingPage {...args} />

export const Primary = Template.bind({})
Primary.args = {
    firstCards: [
        {
            title: 'First card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor:'#403F3F'
        },
        {
            title: 'Second card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor:'#403F3F'
        },
        {
            title: 'Third card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor:'#403F3F'
        },
        {
            title: 'Fourth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor:'#403F3F'
        },
        {
            title: 'Fifth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor:'#403F3F'
        }, {
            title: 'Sixth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor:'#403F3F'
        }
    ],
    secondCards: [
        {
            title: 'First card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor:'#403F3F'
        },
        {
            title: 'Second card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor:'#403F3F'
        },
        {
            title: 'Third card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor:'#403F3F'
        },
        {
            title: 'Fourth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor:'#403F3F'
        },
        {
            title: 'Fifth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor:'#403F3F'
        }, {
            title: 'Sixth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor:'#403F3F'
        }
    ],
    textBoxes: [
        {
            bodyFontSize: 18,
            headerFontSize: 21,
            image: SelfService,
            paragraphs: ['Our most popular and efficient service, customers have access to RxRD.com, on which they scan and create a return inventory. Each step of the process is'],
            title: "Self Service",
            titleColor:'#403F3F'
            

        },
        {
            bodyFontSize: 18,
            headerFontSize: 21,
            image: ExpressService,
            paragraphs: ['This on-site service is available for those pharmacies which only require assistance with the inventory and processing of Schedule II-V Controlled Substances. Once'],
            title: "Express Service",
            titleColor:'#403F3F'
            
        },
        {
            bodyFontSize: 18,
            headerFontSize: 21,
            image: FullService,
            paragraphs: ['Our most comprehensive service, a regional RxRD representative will be assigned to your pharmacy, providing complete on-site account support. These'],
            title: 'Full Service',
            titleColor:'#403F3F'
            
        },
        {
            bodyFontSize: 18,
            headerFontSize: 21,
            image: RxDisposal,
            paragraphs: ["Our RxDisposal service provides you with the responsible, safe and secure destruction of your waste pharmaceuticals. Using our customer returns portal you can'"],
            title: 'RxDisposal',
            titleColor:'#403F3F'
            
        }
    ]
}