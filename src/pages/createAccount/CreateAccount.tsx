import { Button, createStyles, makeStyles, Step, StepConnector, StepIconProps, StepLabel, Theme, Typography } from '@material-ui/core';
import { Container, Card, CardContent, Stepper, withStyles } from '@material-ui/core';
import React from 'react'
import { useStyles } from './CreateAccountStyles';
import { default as BusinessInfo } from '../../assets/BusinessInfo.svg'
import { default as ContactInfo } from '../../assets/ContactInfo.svg'
import { default as PrimaryInfo } from '../../assets/Primary.svg'
import { default as Referral } from '../../assets/Referred.svg'
import { default as BusinessInfoActive } from '../../assets/BusinessInfoActive.svg'
import { default as ContactInfoActive } from '../../assets/ContactInfoActive.svg'
import { default as PrimaryInfoActive } from '../../assets/PrimaryActive.svg'
import { default as ReferralActive } from '../../assets/ReferredActive.svg'
import clsx from 'clsx';

export interface CreateAccountProps {

}

const useColorlibStepIconStyles = makeStyles({
    root: {
        backgroundColor: '#ccc',
        zIndex: 1,
        color: '#fff',
        width: 50,
        height: 50,
        display: 'flex',
        borderRadius: '50%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    active: {
        backgroundImage:
            'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
        boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
    },
    completed: {
        backgroundImage:
            'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
    },
});



const ColorlibConnector = withStyles({
    alternativeLabel: {
        top: 22,
    },
    active: {
        '& $line': {
            backgroundImage:
                'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
        },
    },
    completed: {
        '& $line': {
            backgroundImage:
                'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
        },
    },
    line: {
        height: 3,
        border: 0,
        backgroundColor: '#eaeaf0',
        borderRadius: 1,
    },
})(StepConnector);

const getStepContent = (stepIndex: number) => {
    switch (stepIndex) {
        case 0:
            return 'Select campaign settings...';
        case 1:
            return 'What is an ad group anyways?';
        case 2:
            return 'This is the bit I really care about!';
        default:
            return 'Unknown stepIndex';
    }
}

export const CreateAccount: React.FC<CreateAccountProps> = (props: CreateAccountProps) => {

    const { } = props

    const classes = useStyles(props)

    const steps = ['Business Info', 'Contact Info', 'Primary Wholesaler Info', "Referred By"]
    const [activeStep, setActiveStep] = React.useState(0);

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    function ColorlibStepIcon(props: StepIconProps) {
        const classes = useColorlibStepIconStyles();
        const { active, completed } = props;

        console.log(props)
        const icons: { [index: string]: React.ReactElement } = {
            1: <img src={BusinessInfo} alt="" />,
            2: <img src={ContactInfo} alt="" />,
            3: <img src={PrimaryInfo} alt="" />,
            4: <img src={Referral} alt="" />,
            5: <img src={BusinessInfoActive} alt="" />,
            6: <img src={ContactInfoActive} alt="" />,
            7: <img src={PrimaryInfoActive} alt="" />,
            8: <img src={ReferralActive} alt="" />
        };

        return (
            <div
                className={clsx(classes.root, {
                    [classes.active]: active,
                    [classes.completed]: completed,
                })}
            >
             

                {active || completed ? icons[String(Number(props.icon) + 4 )] : icons[String(props.icon)]}
            </div>
        );
    }

    return (
        <Container>
            <Card>
                <CardContent>
                    <Stepper alternativeLabel activeStep={activeStep} connector={<ColorlibConnector />} >
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    <div>
                        {activeStep === steps.length ? (
                            <div>
                                <Typography className={classes.instructions}>
                                    All steps completed - you&apos;re finished
                                </Typography>
                                <Button onClick={handleReset} className={classes.button}>
                                    Reset
                                </Button>
                            </div>
                        ) : (
                            <div>
                                <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                <div>
                                    <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                                        Back
                                    </Button>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={handleNext}
                                        className={classes.button}
                                    >
                                        {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                    </Button>
                                </div>
                            </div>
                        )}
                    </div>
                </CardContent>
            </Card>
        </Container>
    );
}

