import { ComponentStory } from '@storybook/react';
import { CreateAccount } from './CreateAccount';
export default {
    title: 'Pages/Create Account',
    component: CreateAccount,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof CreateAccount> = args =>  <CreateAccount {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}