import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { OurServicesPageProps } from './OurServicesPage';

export interface OurServicesStylesProps {
    
}
export const useStyles = makeStyles<Theme, OurServicesPageProps>((theme: Theme) =>
    createStyles({
        
    }),
);