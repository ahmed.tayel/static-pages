import { ComponentStory } from '@storybook/react';
import { OurServicesPage } from './OurServicesPage';
import { default as SelfService } from "../../static/Landing page/assets/SelfService.svg";
import { default as RxDisposal } from "../../static/Landing page/assets/RxDisposal.svg";
import { default as FullService } from "../../static/Landing page/assets/FullService.svg";
import { default as ExpressService } from "../../static/Landing page/assets/ExpressService.svg";

export default {
    title: 'Pages/Our Services',
    component: OurServicesPage,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof OurServicesPage> = args =>  <OurServicesPage {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    textBoxes: [
        {
            headerFontSize: 24,
            bodyFontSize: 18,
            image: SelfService,
            paragraphs: ['Our most popular and efficient service, customers have access to RxRD.com, on which they scan and create a return inventory. Each step of the process is guided step-by-step with simple instructions. This helpful online resource is accessible 24/7 to request DEA forms, download inventory reports, print shipping labels, schedule pickup, and track the return to RxRD.', 'In addition to online access, a dedicated account specialist will be assigned to your account. Your account expert will available by phone or email Monday through Friday from 9am-5pm to assist with your returns or answer any questions.'],
            title: "Self Service",
            titleColor: '#403F3F'

        },
        {
            headerFontSize: 24,
            bodyFontSize: 18,
            image: ExpressService,
            paragraphs: ['This on-site service is available for those pharmacies which only require assistance with the inventory and processing of Schedule II-V Controlled Substances. Once you pull expired or unwanted product from your shelves and place them in a box for shipment, a RxRD representative will schedule a visit to your pharmacy to process these items for return. As part of this service, you will be provided a full inventory of the Controlled products, as well as all required DEA 222 forms.'],
            title: "Express Service",
            titleColor: '#403F3F'
        },
        {
            headerFontSize: 24,
            bodyFontSize: 18,
            image: FullService,
            paragraphs: ['Our most comprehensive service, a regional RxRD representative will be assigned to your pharmacy, providing complete on-site account support. These highly-trained field representatives will help you manage your processing timeline and establish an on-going pharmaceuticals return program on your behalf.', 'At each scheduled processing, our returns experts will pull expired or unwanted pharmaceuticals from your shelves, package and label each product for shipping, and complete all required agency forms. In addition, you will receive a full inventory report detailing the value of the products as well as your estimated return value.'],
            title: 'Full Service',
            titleColor: '#403F3F'
        },
        {
            headerFontSize: 24,
            bodyFontSize: 18,
            image: RxDisposal,
            paragraphs: ["Our RxDisposal service provides you with the responsible, safe and secure destruction of your waste pharmaceuticals. Using our customer returns portal you can create your inventory, request a DEA Form 222, print shipping labels and submit payment. The RxDisposal service is suitable for:",""],
            title: 'RxDisposal',
            titleColor: '#403F3F'
        }
    ]
}