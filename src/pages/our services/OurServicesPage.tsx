import { Box, Card, CardContent, Container, Grid } from '@material-ui/core';
import React from 'react'
import { TextBox, TextBoxProps } from '../../atoms/Text Box/TextBox';
import { CardMolecule } from '../../molecules/Card/CardMolecule';
import { FourTextPapers, FourTextPapersProps } from '../../molecules/Four Text Papers/FourTextPapers';
import { useStyles } from './OurServicesStyles';

export interface OurServicesPageProps {
    textBoxes: Array<TextBoxProps>
}

export const OurServicesPage: React.FC<OurServicesPageProps> = (props: OurServicesPageProps) => {
    const { textBoxes } = props

    const classes = useStyles(props)
    return (
        <Container>
            <Card >
                <CardContent>
                    <TextBox title={''} paragraphs={['Rx Reverse Distributors understands the process of returning expired or unwanted pharmaceuticals can be overwhelming and time-consuming. With hundreds of manufacturers, each with different return policies that are constantly changing, the experts at RxRD can manage the entire process to ensure you get the most cash back for your outdated product. Choose from any of the established service levels below, or we can work with you to develop a process that meets the return needs of your pharmacy.']} image={''} titleColor={'#403F3F'} bodyFontSize={18} headerFontSize={40} />
                    <FourTextPapers textBoxes={textBoxes} />
                </CardContent>
            </Card>
        </Container>
    );
}