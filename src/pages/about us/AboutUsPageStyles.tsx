import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface AboutUsPageStylesProps {
    
}
export const useStyles = makeStyles<Theme, AboutUsPageStylesProps>((theme: Theme) =>
    createStyles({
        
    }),
);