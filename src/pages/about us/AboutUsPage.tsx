import { Container } from '@material-ui/core';
import React from 'react'
import { CardMolecule } from '../../molecules/Card/CardMolecule';
import { useStyles } from './AboutUsPageStyles';

export interface AboutUsPageProps {

}

export const AboutUsPage: React.FC<AboutUsPageProps> = (props: AboutUsPageProps) => {
    const { } = props

    const classes = useStyles(props)

    return (
        <Container>

            <CardMolecule titleColor={'#403F3F'} image={''} paragraphs={[`Rx Reverse Distributors, Inc. (RxRD) is an industry leader in pharmaceutical returns processing and disposal. With a reputation for exceptional personalized service and communication, the professionals at RxRD specialize in servicing independently owned pharmacies and small to medium-sized pharmacy chains across the United States.`, `With the development and implementation of proprietary processing and tracking software, the company is a technological innovator in the industry. The easy to use website created by RxRD gives customers real-time access to the returns process from initial inventory to reconciliation.`,`RxRD is fully committed to identifying and recovering all credits due to their customers. In addition to the web portal, detailed communication is provided throughout the process via a dedicated support representative assigned to each account.`,`RxRD is federally licensed by the DEA, DOT, and EPA, as well as the State of Florida and individual states as required. Each employee undergoes extensive background checks and training, ensuring full compliance with government agency regulations.`,`With a company culture focused on operating in accordance with all rules and regulations, RxRD also provides customers with peace of mind knowing their expired products are being disposed of in an environmentally safe and effective manner.`,`As a family-owned and operated company, RxRD also understands the importance of developing long-standing business relationships built on trust and integrity. They believe in full transparency in communication and business practices. RxRD customers know they are receiving the very best in service, consistently exceeding expectations.`]} title={''} />
        </Container>
    );
}