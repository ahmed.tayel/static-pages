import { ComponentStory } from '@storybook/react';
import { AboutUsPage } from './AboutUsPage';
export default {
    title: 'Pages/About Us',
    component: AboutUsPage,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof AboutUsPage> = args =>  <AboutUsPage {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}