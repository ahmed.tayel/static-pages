import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface CompliancePageStylesProps {

}
export const useStyles = makeStyles<Theme, CompliancePageStylesProps>((theme: Theme) =>
    createStyles({
        disclaimer: {
            color: '#A61C14',
            fontSize: 18,
            marginTop: 28
        }
    }),
);