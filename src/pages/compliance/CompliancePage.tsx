import { Card, CardContent, Grid, Typography } from '@material-ui/core';
import React from 'react'
import { PictureAtom, PictureAtomProps } from '../../atoms/Picture/PictureAtom';
import { States, StatesProps } from '../../atoms/States/States';
import { TextBox } from '../../atoms/Text Box/TextBox';
import { useStyles } from './CompliancePageStyles';


export interface CompliancePageProps {
    logos: Array<PictureAtomProps>
    states: Array<StatesProps>
}

export const CompliancePage: React.FC<CompliancePageProps> = (props: CompliancePageProps) => {
    const { logos, states } = props

    const classes = useStyles(props)
    const loadLogos = () => {
        return logos.map((logo) => {
            return (
                <Grid item sm={2} xs={6}>
                    <PictureAtom image={logo.image} altText={logo.altText} height={logo.height} width={logo.width} />
                </Grid>
            )
        })
    }

    const loadStates = () => {
        return states.map((state) => {
            return (
                <Grid item sm={4} xs={12}>
                    <States text={state.text} />
                </Grid>
            )
        })
    }

    return (
        <Card>
            <CardContent>
                <TextBox bodyFontSize={18} headerFontSize={40} image={''} paragraphs={["Rx Reverse Distributors, Inc. is federally licensed by the DEA, EPA and DOT as well as the State of Florida and individual states as required. Our facility is accredited by the National Association of Boards of Pharmacy (NABP). As a Verified Accredited Wholesale Distributor (VAWD) we have undergone a critera compliance review, including a rigourous review of our operating policies and procedures, licensure verification, facility survey and operations, background checks and screening."]} title={''} titleColor={'#403F3F'} />
                <TextBox bodyFontSize={18} headerFontSize={21} image={''} paragraphs={['']} title={'Federal Licenses'} titleColor={'#A61C14'} />
                <Grid container justifyContent={'center'}>
                    {loadLogos()}
                </Grid>
                <TextBox bodyFontSize={18} headerFontSize={21} image={''} paragraphs={['']} title={'State Licenses'} titleColor={'#A61C14'} />
                <Grid container spacing={3}>
                    {loadStates()}
                </Grid>
                <Typography className={classes.disclaimer}>
                        *Rx Reverse Distributors, Inc. provides service in this state.
                        The state board of pharmacy currently does not require a license for reverse distribution.
                    </Typography>
            </CardContent>
        </Card>
    );
}