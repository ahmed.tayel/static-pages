import { ComponentStory } from '@storybook/react';
import { CompliancePage } from './CompliancePage';
import { default as DeaLogo } from "../../static/Compliance/assets/dea_logo.png";
import { default as DotLogo } from "../../static/Compliance/assets/dot_logo.png";
import { default as EpaLogo } from "../../static/Compliance/assets/epa_logo.png";
import { default as FldbprLogo } from "../../static/Compliance/assets/fldbpr_logo.png";
import { default as NabpSeal } from "../../static/Compliance/assets/nabp_seal.png";

export default {
    title: 'Pages/Compliance',
    component: CompliancePage,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof CompliancePage> = args =>  <CompliancePage {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    logos: [
        {
            altText: 'what',
            height: 'auto',
            image: DeaLogo,
            width: 'auto'
        },
        {
            altText: 'what',
            height: 'auto',
            image: DotLogo,
            width: 'auto'
        },
        {
            altText: 'what',
            height: 'auto',
            image: EpaLogo,
            width: 'auto'
        },
        {
            altText: 'what',
            height: 'auto',
            image: FldbprLogo,
            width: 'auto'
        },
        {
            altText: 'what',
            height: 'auto',
            image: NabpSeal,
            width: 'auto'
        }
    ],
    states: [
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
        {
            text:'Alabama'
        },
    ]
}