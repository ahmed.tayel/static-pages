import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface WhyChooseUsPageStylesProps {
    
}
export const useStyles = makeStyles<Theme, WhyChooseUsPageStylesProps>((theme: Theme) =>
    createStyles({
        
    }),
);