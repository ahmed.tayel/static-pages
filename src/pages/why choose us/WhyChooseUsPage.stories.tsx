import { ComponentStory } from '@storybook/react';
import { WhyChooseUsPage } from './WhyChooseUsPage';
export default {
    title: 'Pages/Why Choose Us',
    component: WhyChooseUsPage,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof WhyChooseUsPage> = args =>  <WhyChooseUsPage {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}