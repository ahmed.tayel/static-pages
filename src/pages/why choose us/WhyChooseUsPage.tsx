import { Card, CardContent, Container } from '@material-ui/core';
import React from 'react'
import { TextBox } from '../../atoms/Text Box/TextBox';
import { TwoColumnCards } from '../../molecules/2-Column Cards/TwoColumnCards';
import { default as CompReport } from "../../static/Why Choose Us/assets/ComprehensiveReporting.svg";
import { default as Minimium } from "../../static/Why Choose Us/assets/Minimum.svg";
import { default as RapidTurnaround } from "../../static/Why Choose Us/assets/RapidTurnaround.svg";
import { default as UnparalleledSupport } from "../../static/Why Choose Us/assets/UnparalleledSupport.svg";
import { default as Technology } from "../../static/Why Choose Us/assets/Technology.svg";
import { default as Programs } from "../../static/Why Choose Us/assets/PersonalizedPrograms.svg";

export interface WhyChooseUsPageProps {

}

export const WhyChooseUsPage: React.FC<WhyChooseUsPageProps> = (props: WhyChooseUsPageProps) => {
    const { } = props

    return (
        <Container>
            <Card>
                <CardContent>
                    <TextBox title={''} image={''} bodyFontSize={18} headerFontSize={21} paragraphs={['Many times, making a change in your pharmaceutical returns process could result in an increase of thousands of dollars in cash back to your pharmacy.', 'Rx Reverse Distributors has earned a reputation in the industry for maximizing return credit on all expiring or unwanted pharmaceuticals, while making the process surprisingly uncomplicated.']} titleColor={'#403F3F'} />
                    <TwoColumnCards cards={[
                        {
                            image: Minimium,
                            paragraphs: ["Unlike other return services in the industry, RxRD is exceedingly persistent in identifying and recovering the maximum amount of cash credit for your expired and unwanted pharmaceuticals. RxRD makes the process simple with no product exclusions, no invoices, no out-of-pocket expenses, and no contract terms."],
                            title: 'Maximum Return, Minimum Hassle',
                            titleColor: '#403F3F'
                        },
                        {
                            image: UnparalleledSupport,
                            paragraphs: ["Every RxRD customer is assigned a dedicated support representative who will regularly analyze your account to maximize your return value. These service professionals are available to answer questions, alert you of return timelines, and assist in the inventory and tracking process."],
                            title: 'Unparalleled Support',
                            titleColor: '#403F3F'
                        },
                        {
                            image: RapidTurnaround,
                            paragraphs: ["Through leading-edge tracking technology and efficient internal processing practices, RxRD typically processes pharmaceutical returns quicker than the industry average. RxRD has long-standing partnerships with the Nation's leading manufacturers, wholesalers, and buying groups. The RxRD accounting team will follow-up with ALL of them until you get the full the credit possible for returned products."],
                            title: 'Rapid Turnaround',
                            titleColor: '#403F3F'
                        },
                        {
                            image: CompReport,
                            paragraphs: ["All transactions with RxRD are transparent and uncomplicated. You will be provided with easy-to-read inventory reports detailing your estimated cash return amount once we receive your shipment. After our team works diligently to get the maximum amount of cash from your returns, you will also receive a monthly cash statement, eliminating confusing wholesaler reconciliation reports."],
                            title: 'Comprehensive Reporting',
                            titleColor: '#403F3F'
                        },
                        {
                            image: Technology,
                            paragraphs: ["As an innovator in pharmaceutical returns and inventory technology, RxRD develops software and online applications that are simple for anyone to use. For those customers using the online portal, the returns process is guided step-by-step. There, you can download forms and shipping labels, print reports, schedule pickup, and even track the progress of your return."],
                            title: 'User-Friendly Technology',
                            titleColor: '#403F3F'
                        },
                        {
                            image: Programs,
                            paragraphs: ["As a family-owned and customer-focused company, RxRD offers various levels of service depending on the needs of your pharmacy. With the convenience of in-house or on-site services, the specialists at RxRD can help you find the program that is the right fit for your operation and budget."],
                            title: 'Personalized Programs',
                            titleColor: '#403F3F'
                        },
                    ]} />
                </CardContent>
            </Card>
        </Container>
    );
}