import { Card, CardContent, Container, Grid, Paper, SvgIcon, Typography } from '@material-ui/core';
import React from 'react'
import { PictureAtom } from '../../atoms/Picture/PictureAtom';
import { useStyles } from './ContactUsPageStyles';
import { default as GoogleMap } from "../../static/Contact us/assets/Capture.png";
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';

export interface ContactUsPageProps {

}

export const ContactUsPage: React.FC<ContactUsPageProps> = (props: ContactUsPageProps) => {
    const { } = props

    const classes = useStyles(props)

    return (
        <Container>
            <Card>
                <CardContent>
                    <Container>
                        <PictureAtom altText={"auto"} height={"auto"} image={GoogleMap} width={"auto"} />
                    </Container>
                    <Grid container>
                        <Grid item sm={4}>
                            <Paper className={classes.paper} elevation={0}>
                                <SvgIcon component={BusinessCenterIcon} className={classes.icon} />
                                <Typography className={classes.info}>
                                    Rx REVERSE DISTRIBUTORS, INC. 9255 US Highway 1 Sebastian, FL 32958
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={4}>
                            <Paper className={classes.paper} elevation={0}>
                                <SvgIcon component={BusinessCenterIcon} className={classes.icon} />
                                <Typography className={classes.info}>
                                    866-388-7973
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={4}>
                        </Grid>
                        <Grid item sm={4}>
                            <Paper className={classes.paper} elevation={0}>
                                <SvgIcon component={PhoneIcon} className={classes.icon} />
                                <Typography className={classes.info}>
                                    866-388-7973
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={4}>
                            <Paper className={classes.paper} elevation={0}>
                                <SvgIcon component={EmailIcon} className={classes.icon} />
                                <Typography className={classes.info}>
                                    Contact Email
                                </Typography>
                            </Paper>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </Container>
    );
}