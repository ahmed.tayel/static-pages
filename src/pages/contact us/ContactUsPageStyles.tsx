import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface ContactUsPageStylesProps {
    
}
export const useStyles = makeStyles<Theme, ContactUsPageStylesProps>((theme: Theme) =>
    createStyles({
        paper: {
            padding: theme.spacing(2),
            textAlign: 'left',
            color: theme.palette.text.secondary,
            display: 'flex',
        },
        icon: {
            color: '#323A87'
        },
        info: {
            fontSize: 21,
            color: '#403F3F',
            marginLeft: 8
        },
    }),
);