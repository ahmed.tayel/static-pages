import { ComponentStory } from '@storybook/react';
import { ContactUsPage } from './ContactUsPage';
export default {
    title: 'Pages/Contact Us',
    component: ContactUsPage,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof ContactUsPage> = args =>  <ContactUsPage {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}