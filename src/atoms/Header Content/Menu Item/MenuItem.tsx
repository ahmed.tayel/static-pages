import { Link } from '@material-ui/core';
import React from 'react'
import { useStyles } from './MenuItemStyles';

export interface MenuItemProps {
    text: string,
    underline: "none" | "always" | "hover" | undefined
}

export const MenuItem: React.FC<MenuItemProps> = (props: MenuItemProps) => {
    const { text , underline} = props
    const { menuItems } = useStyles(props)
    return (
        <Link underline={underline} href="#" className={menuItems}>{text}</Link>
    );
}