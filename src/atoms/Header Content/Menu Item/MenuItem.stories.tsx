import { ComponentStory } from '@storybook/react';
import { MenuItem } from './MenuItem';
import { default as LandingPageBackground } from '../../../assets/banner.png'
export default {
    title: 'Atoms/NavbarMenuItem',
    component: MenuItem,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof MenuItem> = args =>  <MenuItem {...args}/>
    
export const Item = Template.bind({})
Item.args = {
    text: "Text",
    underline: "none"
}
export const SelectedItem = Template.bind({})
SelectedItem.args = {
    text: "Text",
    underline:"always"
}
export const HoveredItem = Template.bind({})
HoveredItem.args = {
    text: "Text",
    underline:"hover"
}

