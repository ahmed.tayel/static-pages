import { ComponentStory } from '@storybook/react';
import { Logo } from './Logo';

export default {
    title: 'Atoms/Logo',
    component: Logo,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof Logo> = args =>  <Logo {...args}/>
    
export const Large = Template.bind({})
Large.args = {
    height: "200px",
    width: "200px"
}
export const Medium = Template.bind({})
Medium.args = {
    height: "100px",
    width: "100px"
}
export const Small = Template.bind({})
Small.args = {
    height: "50px",
    width: "50px"
}