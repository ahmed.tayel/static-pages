import React from 'react'
import { default as LogoIcon } from '../../../assets/logo.svg'
import { useStyles } from './LogoStyles'

export interface LogoProps {
    height: string,
    width: string
}

export const Logo: React.FC<LogoProps> = (props: LogoProps) => {
    const { height, width } = props

    const {image} = useStyles(props)

    return (
        <div > 
            <img className={image} src={ LogoIcon } alt="" />
        </div>
    );
}