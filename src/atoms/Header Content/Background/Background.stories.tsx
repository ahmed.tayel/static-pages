import { ComponentStory } from '@storybook/react';
import { Background } from './Background';
import { default as Home } from "../../../assets/banner.png"
import { default as ComplianceBackground } from "../../../assets/compliance.png"
import { default as TermsAndConditionsBackground } from "../../../assets/termsandconditions.png"
import { default as ContactUsBackground } from "../../../assets/contactus.png"
import { default as ReturnRequestBackground } from "../../../assets/returnrequest.png"



export default {
    title: 'Atoms/Background',
    component: Background,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof Background> = args =>  <Background {...args}/>
    
export const LandingPage = Template.bind({})
LandingPage.args = {
    imageUrl: Home
}
export const Compliance = Template.bind({})
Compliance.args = {
    imageUrl: ComplianceBackground
}
export const TermsAndConditions = Template.bind({})
TermsAndConditions.args = {
    imageUrl: TermsAndConditionsBackground
}
export const ContactUs = Template.bind({})
ContactUs.args = {
    imageUrl: ContactUsBackground
}
export const ReturnRequest = Template.bind({})
ReturnRequest.args = {
    imageUrl: ReturnRequestBackground
}