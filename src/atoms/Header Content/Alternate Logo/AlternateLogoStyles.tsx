import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { AlternateLogoProps } from './AlternateLogo';

export interface AlternateLogoStylesStyleProps {
    image: BaseCSSProperties
}
export const useStyles = makeStyles<Theme, AlternateLogoProps>((theme: Theme) =>
    createStyles({
        image: (props: AlternateLogoProps) => ({
            width: props.width,
            height: props.height
         }),    
    }),
);