import { ComponentStory } from '@storybook/react';
import { AlternateLogo } from './AlternateLogo';

export default {
    title: 'Atoms/AlternateLogo',
    component: AlternateLogo,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof AlternateLogo> = args =>  <AlternateLogo {...args}/>
    
export const Large = Template.bind({})
Large.args = {
        width: "200px",
        height: "200px"
}
export const Medium = Template.bind({})
Medium.args = {
        width: "100px",
        height: "100px"
}
export const Small = Template.bind({})
Small.args = {
        width: "50px",
        height: "50px"
}