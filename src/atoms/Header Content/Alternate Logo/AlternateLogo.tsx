import React from 'react'
import { default as Logo } from "../../../assets/logoSmall.svg"
import { useStyles } from './AlternateLogoStyles'

export interface AlternateLogoProps {
    height: string,
    width: string
}

export const AlternateLogo: React.FC<AlternateLogoProps> = (props: AlternateLogoProps) => {
    const { height, width } = props

    const {image} = useStyles(props)

    return (
        <div>
            <img className={image} src={Logo} alt="" />
        </div>
    );
}