import { ComponentStory } from '@storybook/react';
import { TextButton } from './TextButton';
export default {
    title: 'Atoms/TextButton',
    component: TextButton,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof TextButton> = args =>  <TextButton {...args}/>
    
export const Normal = Template.bind({})
Normal.args = {
    text: "Click Here",
    version: 1
    
}

export const Small = Template.bind({})
Small.args = {
    text: "Click Here",
    version: 2
}