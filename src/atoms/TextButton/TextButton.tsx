import { Button } from '@material-ui/core';
import React from 'react'
import { useStyles } from './TextButtonStyles';

export interface TextButtonProps {
    text: string
    version: 1 | 2 | undefined
}

export const TextButton: React.FC<TextButtonProps> = (props: TextButtonProps) => {

    const {version,  text } = props

    const classes = useStyles(props)
    return (
        <div>
            <Button disableRipple variant="text" className={version === 1 ? classes.mainContentButton : classes.button}>
                {text}
            </Button>
        </div>
    );
}