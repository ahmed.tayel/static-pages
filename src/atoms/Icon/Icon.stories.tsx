import { ComponentStory } from '@storybook/react';
import { IconAtom } from './IconAtom';
import { default as TelephoneIcon } from '../../assets/telephone.svg'
import { default as BriefcaseIcon } from '../../assets/briefcase.svg'

export default {
    title: 'Atoms/Icon',
    component: IconAtom,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof IconAtom> = args =>  <IconAtom {...args}/>
    
export const Telephone = Template.bind({})
Telephone.args = {
    icon: TelephoneIcon
}
export const Briefcase = Template.bind({})
Briefcase.args = {
    icon: BriefcaseIcon
}