import React from 'react'
import { useStyles } from './IconStyles';

export interface IconAtomProps {

    icon: string,
    altText: string
}

export const IconAtom: React.FC<IconAtomProps> = (props: IconAtomProps) => {
    const { icon, altText} = props
    
    const classes = useStyles(props)
    return (
        <div>
            <div className={classes.icon}>
                <img src={icon} alt={altText} />
            </div>
        </div>
    );
}