import { ComponentStory } from '@storybook/react';
import { PictureAtom } from './PictureAtom';
import { default as Image } from '../../assets/banner.png'

export default {
    title: 'Atoms/Picture',
    component: PictureAtom,
    argTypes: {

    }
}

const Template: ComponentStory<typeof PictureAtom> = args => <PictureAtom {...args} />

export const Primary = Template.bind({})
Primary.args = {
    image: Image,
    altText: 'Image'
}