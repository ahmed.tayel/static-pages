import React from 'react'
import { Button } from "@material-ui/core";
import { useStyles } from "./HeaderButtonStyles";



export interface ButtonProps {
    label: string,
    textColor: string,
    width: string,
    icon: string,
    background: string,
    fontSize: string
}

export const HeaderButton: React.FC<ButtonProps> = (props: ButtonProps) => {
    const { label = 'Button', textColor, width, icon } = props
    const classes = useStyles(props)

    const handleClick = () => {

    }
    return (
        <Button onClick={handleClick} variant="text" startIcon={<img src={icon} />} className={classes.button} >
            {label}
        </Button>
    );
}