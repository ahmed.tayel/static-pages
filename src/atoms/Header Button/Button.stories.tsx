import { ComponentStory } from "@storybook/react/dist/ts3.9/client/preview/types-6-3";
import { HeaderButton } from "./HeaderButton";
import { default as Headphone } from "../../assets/headphone.svg";
import { default as UserIcon } from '../../assets/userIcon.svg';
import { default as LoginIcon } from '../../assets/loginIcon.svg';

export default {
    title: 'Atoms/IconButtons',
    component: HeaderButton,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof HeaderButton> = args =>  <HeaderButton {...args}/>

export const Primary = Template.bind({})
Primary.args = {
    label: 'Register',
    textColor: '#fff',
    fontSize: '21',
    icon: UserIcon
}
export const Seconday = Template.bind({})
Seconday.args = {
    label: 'Contact Us',
    textColor: '#fff',
    fontSize: '19',
    icon: Headphone
}
export const Lastly = Template.bind({})
Lastly.args = {
    label: 'Login',
    width: '188px',
    background: 'transparent linear-gradient(107deg, #A61C14 0%, #530E0A 100%) 0% 0% no-repeat padding-box',
    textColor: '#fff',
    fontSize: '21',
    icon: LoginIcon
    
}