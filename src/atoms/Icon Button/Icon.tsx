import { IconButton } from '@material-ui/core';
import React from 'react'

interface IconProps {
    icon: string
}

export const Icon: React.FC<IconProps> = (props: IconProps) => {
    const { icon } = props

    return (
        <IconButton>
            <img src={icon} alt="" />
        </IconButton>
    );
}