
import { Icon } from "./Icon";
import { ComponentStory } from "@storybook/react";
import { default as Burger } from '../../assets/burger.svg';
import { default as LogoText } from '../../assets/logoText.svg';
import { default as UserIcon } from '../../assets/userIcon.svg';
import { default as LoginIcon } from '../../assets/loginIcon.svg';


export default {
    title: 'Atoms/Header/IconOnlyButton',
    component: Icon,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof Icon> = args =>  <Icon {...args}/>

export const User = Template.bind({})
User.args = {
    icon: UserIcon
}
export const Login = Template.bind({})
Login.args = {
    icon: LoginIcon
}
export const BurgerIcon = Template.bind({})
BurgerIcon.args = {
    icon: Burger
}
