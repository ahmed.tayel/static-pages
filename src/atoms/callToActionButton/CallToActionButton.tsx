import { Button } from '@material-ui/core';
import React from 'react'
import { useStyles } from './CallToActionButtonStyles';

export interface CallToActionButtonProps {
    text: string,
    width: string,
    height: string
}

export const CallToActionButton: React.FC<CallToActionButtonProps> = (props: CallToActionButtonProps) => {
    
    const { height, width, text } = props

    const classes = useStyles(props)
    
    return (
        <Button className={classes.button} disableElevation variant="contained" >{text}</Button>
    );
}