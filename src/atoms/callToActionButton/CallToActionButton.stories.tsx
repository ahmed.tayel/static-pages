import { ComponentStory } from '@storybook/react';
import { CallToActionButton } from './CallToActionButton';
export default {
    title: 'Atoms/CallToActionButton',
    component: CallToActionButton,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof CallToActionButton> = args =>  <CallToActionButton {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
     text: "Call To Action",
     width: '897px',
     height: '90px'
}