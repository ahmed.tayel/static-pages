import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { LinkAtomProps } from './LinkAtom';

export interface LinkStylesProps {
    
}
export const useStyles = makeStyles<Theme, LinkAtomProps>((theme: Theme) =>
    createStyles({
        content: {
            fontSize: 21,
            textTransform: 'capitalize',
            borderRadius: 15,
            color: '#ffffff',
            justifyContent:'flex-start',
            '&:hover': {
                background: 'transparent' 
            },
            '&:active': {
                background: 'transparent'
            }
        },
    }),
);