import { Button } from '@material-ui/core';
import React from 'react'
import { useStyles } from './LinkStyles';

export interface LinkAtomProps {
    text: string,
    onClick: React.MouseEventHandler
}

export const LinkAtom: React.FC<LinkAtomProps> = (props: LinkAtomProps) => {
    
    const { text, onClick} = props
    
    const classes = useStyles(props)
    return (
        <Button onClick={onClick} disableRipple className={classes.content}>
            {text}
        </Button>
    );
}