import { ComponentStory } from '@storybook/react';
import { LinkAtom } from './LinkAtom';
export default {
    title: 'Atoms/Link',
    component: LinkAtom,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof LinkAtom> = args =>  <LinkAtom {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    text: "About us",
    onClick: () => {
        console.log(2)
    }
}