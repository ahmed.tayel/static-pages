import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { StatesProps } from './States';

export interface StatesStylesProps {

}
export const useStyles = makeStyles<Theme, StatesProps>((theme: Theme) =>
    createStyles({
        paper: {
            padding: theme.spacing(2),
            textAlign: 'left',
            backgroundColor: '#EEEEEE',
            display: 'flex',
            alignItems: 'center',
            [theme.breakpoints.down('xs')]: {
                justifyContent: 'center'
            }
        },
        paperText: {
            fontSize: 21,
            color: '#403F3F',
            paddingLeft: 55,
            [theme.breakpoints.down('xs')]: {
                paddingLeft: 0
            }
        },
    }),
);