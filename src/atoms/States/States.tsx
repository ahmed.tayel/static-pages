import { Paper, Typography } from '@material-ui/core';
import React from 'react'
import { useStyles } from './StatesStyles';

export interface StatesProps {
    text: string
}

export const States: React.FC<StatesProps> = (props: StatesProps) => {
    const { text } = props

    const classes = useStyles(props)
    return (
        <Paper className={classes.paper} elevation={0} >
            <Typography className={classes.paperText}>
                {text}
            </Typography>
        </Paper>
    );
}