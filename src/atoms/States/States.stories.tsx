import { ComponentStory } from '@storybook/react';
import { States } from './States';
export default {
    title: 'Atoms/States',
    component: States,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof States> = args =>  <States {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    text: "Alabama"
}