import { Paper, Typography } from '@material-ui/core';
import React from 'react'
import { useStyles } from './TextBoxStyles';

export interface TextBoxProps {
    image: string,
    title: string,
    paragraphs: Array<string>,
    titleColor: '#403F3F' | '#A61C14' | undefined,
    bodyFontSize: 18 | 20 | undefined,
    headerFontSize: 40 | 24 | 21 | 32
}

export const TextBox: React.FC<TextBoxProps> = (props: TextBoxProps) => {
    const { paragraphs, title, image } = props

    const classes = useStyles(props)

    const loadParagraphs = () => {

        return paragraphs.map((paragraph) => {
            return (
                <div>
                    <Typography className={classes.paragraph}>
                        {paragraph}
                    </Typography>
                    <br />
                </div>
            )
        })
    }
    return (
        <div>
            <Paper elevation={0} className={classes.paper}>
                {image !== '' ? <img src={image} alt="" /> : ''}
                <Typography className={classes.title} gutterBottom>
                    {title}
                </Typography>
                {loadParagraphs()}
            </Paper>
        </div>
    );
}