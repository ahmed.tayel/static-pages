import { ComponentStory } from '@storybook/react';
import { TextBox } from './TextBox';
import { default as RxDisposal } from "../../static/Our Services/assets/RxDisposal.svg";
import { WifiSharp } from '@material-ui/icons';

export default {
    title: 'Atoms/TextBox',
    component: TextBox,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof TextBox> = args =>  <TextBox {...args}/>
    
export const RedTitle = Template.bind({})
RedTitle.args = {
    image: '',
    title: 'Title',
    titleColor: '#A61C14',
    paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin interdum dui. Proin posuere libero dui, ac semper enim consequat a. Fusce vulputate venenatis augue. Etiam sed nulla ut nibh bibendum semper ac non arcu. In faucibus pellentesque ipsum, sit amet facilisis ipsum eleifend vitae. Etiam id scelerisque purus. Praesent id dui nec sem facilisis blandit nec ac leo. Curabitur ac augue et libero convallis semper ut eget metus. Maecenas porta eget est non sollicitudin. Ut porttitor, risus eu imperdiet malesuada, mi magna eleifend dui, ac vestibulum arcu diam ac nisl. Proin blandit a nunc et consequat.'],
    bodyFontSize: 18
}
export const BlackTitle = Template.bind({})
BlackTitle.args = {
    image: '',
    title: 'Title',
    titleColor: '#403F3F',
    paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin interdum dui. Proin posuere libero dui, ac semper enim consequat a. Fusce vulputate venenatis augue. Etiam sed nulla ut nibh bibendum semper ac non arcu. In faucibus pellentesque ipsum, sit amet facilisis ipsum eleifend vitae. Etiam id scelerisque purus. Praesent id dui nec sem facilisis blandit nec ac leo. Curabitur ac augue et libero convallis semper ut eget metus. Maecenas porta eget est non sollicitudin. Ut porttitor, risus eu imperdiet malesuada, mi magna eleifend dui, ac vestibulum arcu diam ac nisl. Proin blandit a nunc et consequat.'],
    bodyFontSize: 18,
    
}