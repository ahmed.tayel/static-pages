import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { TextBoxProps } from './TextBox';

export interface TextBoxStylesProps {
    paper: BaseCSSProperties,
    title: BaseCSSProperties,
    paragraph: BaseCSSProperties
}
export const useStyles = makeStyles<Theme, TextBoxProps>((theme: Theme) =>
    createStyles({
        paper: {
            textAlign: 'left',
            [theme.breakpoints.down('xs')]: {
                
            }
        },
        title: (props: TextBoxProps) => ({
            fontSize: props.headerFontSize,
            color: props.titleColor,
            fontWeight: 'bold',
            [theme.breakpoints.down('xs')]: {
                fontSize:20
            }
        }),
        paragraph: (props: TextBoxProps) => ({
            fontSize: props.bodyFontSize,
            color: props.titleColor === '#A61C14' ? "#403F3F" : "#707070",
            [theme.breakpoints.down('xs')]: {
             fontSize: 16   
            }
        }),

    }),
);