import { IconButton } from '@material-ui/core';
import React from 'react'
import { useStyles } from './IconButtonStyles';

export interface IconButtonProps {

    icon: string,
    onClick: React.MouseEventHandler
}

export const IconButtonAtom: React.FC<IconButtonProps> = (props: IconButtonProps) => {
    const { icon, onClick } = props

    const classes = useStyles(props)
    return (
        <div>
            <IconButton onClick={onClick}>
                <img src={icon} alt="Twitter social" />
            </IconButton>
        </div>
    );
}