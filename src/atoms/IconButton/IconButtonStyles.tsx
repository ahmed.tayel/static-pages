import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { IconButtonProps } from './IconButtonAtom';

export interface IconButtonStylesProps {
    
}
export const useStyles = makeStyles<Theme, IconButtonProps>((theme: Theme) =>
    createStyles({
        
    }),
);