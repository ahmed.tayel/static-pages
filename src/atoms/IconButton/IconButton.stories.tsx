import { ComponentStory } from '@storybook/react';
import { IconButtonAtom } from './IconButtonAtom';
import { default as Twitter } from '../../assets/twitter.svg'

export default {
    title: 'Atoms/IconButton',
    component: IconButtonAtom,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof IconButtonAtom> = args =>  <IconButtonAtom {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    icon: Twitter,
    onClick: () => {
        console.log(2)
    }
}