import { TableCell } from '@material-ui/core';
import { Button } from '@material-ui/core';
import React from 'react'
import { useStyles } from './TableCellAtomStyles';

export interface TableCellAtomProps {
    text: string,
    clickable: boolean,
    onClick: React.MouseEventHandler | undefined,
    header: boolean,
    bold: boolean
}

export const TableCellAtom: React.FC<TableCellAtomProps> = (props: TableCellAtomProps) => {
    const { header, text, clickable, onClick } = props

    const classes = useStyles(props)
    if (clickable)
        return (
                <TableCell className={header ? classes.head : classes.body} align="center"><Button className={classes.button} onClick={onClick} disableRipple>{text}</Button></TableCell>
            )
        else
        return (
            
                <TableCell className={header ? classes.head : classes.body} align="center">{text}</TableCell>
          
        )
}