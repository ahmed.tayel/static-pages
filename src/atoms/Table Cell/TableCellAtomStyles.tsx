import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { TableCellAtomProps } from './TableCellAtom';

export interface TableCellAtomStylesProps {

}
export const useStyles = makeStyles<Theme, TableCellAtomProps>((theme: Theme) =>
    createStyles({
        head: {
            backgroundColor: '#323A87',
            color: theme.palette.common.white,
            fontSize: 18
        },
        body: (props: TableCellAtomProps) => ({
            fontSize: 14,
            fontWeight: props.bold ? 'bold' : undefined
        }),
        button: (props: TableCellAtomProps) => ({
            color: props.header ? "red" : "red",
            textTransform: "capitalize",
            textDecoration: 'underline'
        })
    }),

);