import { ComponentStory } from '@storybook/react';
import { TableCellAtom } from './TableCellAtom';
export default {
    title: 'Atoms/TableCell',
    component: TableCellAtom,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof TableCellAtom> = args =>  <TableCellAtom {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    text: 'click me'
}