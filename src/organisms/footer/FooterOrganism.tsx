import { Button, Divider, IconButton, Typography } from '@material-ui/core';

import React from 'react'

import { useStyles } from './FooterOrganismStyles';
import { default as LogoIcon } from '../../assets/logo.svg'
import { default as BriefcaseIcon } from '../../assets/briefcase.svg'
import { default as Mail } from '../../assets/mail.svg'
import { default as Telephone } from '../../assets/telephone.svg'
import { default as Fax } from '../../assets/fax.svg'
import { default as Facebook } from '../../assets/facebook.svg'
import { default as Instagram } from '../../assets/instagram.svg'
import { default as Twitter } from '../../assets/twitter.svg'
import { Logo } from '../../atoms/Header Content/Logo/Logo';
import { TextIcon } from '../../molecules/Text with Icon/TextIcon';
import { TextButton } from '../../atoms/TextButton/TextButton';
import { IconButtonAtom } from '../../atoms/IconButton/IconButtonAtom';

export interface FooterOrganismProps {

}

export const FooterOrganism: React.FC<FooterOrganismProps> = (props: FooterOrganismProps) => {
    const { } = props

    const classes = useStyles(props)

    return (
        <div className={classes.root}>
            <div className={classes.sectionLogo}>
                <Logo height={''} width={''} />
            </div>
            <Divider className={classes.divider} orientation={window.screen.availWidth > 300 ? 'vertical' : 'horizontal'} />
            <div className={classes.sectionContent}>
                <Typography className={classes.subtitle}>
                    Contact us
                </Typography>
                <div className={classes.entry}>
                    <TextIcon altText={'sd'} icon={BriefcaseIcon} text={"Rx REVERSE DISTRIBUTORS, INC. 9255 US Highway 1 Sebastian, FL 32958"} />
                </div>
                <div className={classes.entry}>
                    <TextIcon altText={'Phone number'} icon={Telephone} text={"866-388-7973"} />
                </div>
                <div className={classes.entry}>
                    <TextIcon altText={'Fax number'} icon={Fax} text={"772-388-1260"} />
                </div>
                <div className={classes.entry}>
                    <TextIcon altText={'Email'} icon={Mail} text={"Contact Email"} />
                </div>
            </div>
            <Divider className={classes.divider} orientation={window.screen.availWidth > 300 ? 'vertical' : 'horizontal'} />
            <div className={classes.sectionLinks}>
                <Typography className={classes.subtitle}>
                    Quick Links
                </Typography>
                <Button disableRipple className={classes.content}>
                    About Us
                </Button>
                <Button disableRipple className={classes.content}>
                    Services
                </Button>
                <Button disableRipple className={classes.content}>
                    Contact Us
                </Button>
                <Button disableRipple className={classes.content}>
                    Managment
                </Button>
                <Button disableRipple className={classes.content}>
                    Compliance
                </Button>

            </div>
            <Divider className={classes.divider} orientation={window.screen.availWidth > 300 ? 'vertical' : 'horizontal'} />
            <div className={classes.sectionContent}>
               <TextButton text={'Start Return'} version={2} />
                <div className={classes.socials}>
                    <IconButtonAtom icon={Twitter} onClick={()=>{}}/>
                    <IconButtonAtom icon={Facebook} onClick={()=>{}}/>
                    <IconButtonAtom icon={Instagram} onClick={()=>{}}/>
                </div>
            </div>
        </div>
    );
}