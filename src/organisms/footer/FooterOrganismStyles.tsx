import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { FooterOrganismProps } from './FooterOrganism';

export interface FooterOrganismStylesProps {
    
}
export const useStyles = makeStyles<Theme, FooterOrganismProps>((theme: Theme) =>
    createStyles({
        root: {
            minWidth: 275,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            background: 'transparent linear-gradient(180deg, #323A87 0%, #211D57 100%) 0% 0% no-repeat padding-box',
            height: 466,
            [theme.breakpoints.down('xs')]: {
                flexDirection: 'column',
                height: 1200,
            }
        },
        divider: {
            color: '#FFFFFF',
            maxHeight: 236,
            backgroundColor: '#FFFFFF',
            [theme.breakpoints.down('xs')] : {
                height:1,
                width: '60%',
                
            }
        },
        subtitle: {
            fontSize: 26,
            fontWeight: 'bold',
            textAlign: 'left',
            color: '#ffffff'
        },
        sectionLogo: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            margin: theme.spacing(5),
            flexDirection: 'column',
            maxHeight: 200,
            [theme.breakpoints.down('xs')]: {
                
            }
        },
        sectionContent: {
            display: 'flex',
            alignItems: 'left',
            justifyContent: 'flex-start',
            margin: theme.spacing(5),
            flexDirection: 'column',
            color: '#ffffff',
            [theme.breakpoints.down('xs')]: {
                
            }
        },
        sectionLinks: {
            display: 'flex',
            alignItems: 'left',
            justifyContent: 'flex-start',
            margin: theme.spacing(5),
            flexDirection: 'column',
            color: '#ffffff',
            width: 340,
            marginLeft: theme.spacing(12)
        },
        content: {
            fontSize: 21,
            textTransform: 'capitalize',
            borderRadius: 15,
            color: '#ffffff',
            justifyContent:'flex-start',
            '&:hover': {
                background: 'transparent' 
            },
            '&:active': {
                background: 'transparent'
            }
        },
        entry: {
            display: 'flex',
            alignItems: 'center',
            alignContent: 'left',
            justifyContent: 'flex-start',
            textAlign: 'left',
            margin: theme.spacing(1)
        },
        icon: {
            marginRight: theme.spacing(2)
        },
        button: {
            width: 280,
            height: 58,
            border: '2px solid #ffffff',
            color: '#ffffff',
            textTransform: 'capitalize',
            borderRadius: 15,
            fontSize: 24,

        },
        socials: {
            display: 'flex',
            justifyContent: 'center',
            margin: theme.spacing(1)
            
        }
    }),
);