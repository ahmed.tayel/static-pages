import { ComponentStory } from '@storybook/react';
import { FooterOrganism } from './FooterOrganism';
export default {
    title: 'Organisms/Footer',
    component: FooterOrganism,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof FooterOrganism> = args =>  <FooterOrganism {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}