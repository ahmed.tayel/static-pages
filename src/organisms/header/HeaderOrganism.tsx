import { AppBar, Toolbar, Button, Container, Typography, Link } from '@material-ui/core';
import React, { useEffect, useState } from 'react'

import { HeaderButton } from '../../atoms/Header Button/HeaderButton';
import { useStyles } from './HeaderOrganismStyles';
import { default as LoginIcon } from '../../assets/loginIcon.svg';
import { default as UserIcon } from '../../assets/userIcon.svg';
import { default as Banner } from "../../assets/banner.png";
import { default as Headphone } from "../../assets/headphone.svg";
import { default as LogoIcon } from "../../assets/logoSmall.svg";
import { TextButton } from '../../atoms/TextButton/TextButton';
import { HeaderMolecule } from '../../molecules/Header/HeaderMolecule';
import { Background } from '../../atoms/Header Content/Background/Background';
import { Logo } from '../../atoms/Header Content/Logo/Logo';
import { HeaderMenu } from '../../molecules/Header Content/Header Menu/HeaderMenu';
import { HeaderNavbar } from '../../molecules/Header Content/Header Logo + Menu/HeaderNavbar';



export interface HeaderOrganismProps {

}

export const HeaderOrganism: React.FC<HeaderOrganismProps> = (props: HeaderOrganismProps) => {
    const { } = props

    const classes = useStyles(props)
    const [windowSize, setWindowSize] = useState<boolean>(window.screen.availWidth > 500);
    useEffect(() => {
        const handleWindowResize = () => setWindowSize(window.innerWidth > 500 ? true : false);
        window.addEventListener("resize", handleWindowResize);


        return () => window.removeEventListener("resize", handleWindowResize);
    }, []);
    return (
        <div>

            <HeaderMolecule />
            <Background imageUrl={Banner} >
                <div className={classes.logoIcon}>
                    <HeaderNavbar />
                </div>
                <div className={classes.mainContent}>
                    <Typography className={classes.mainContentText}>
                        We do all the work. You get the Credit!
                    </Typography>
                    <TextButton text={'Start Return'} version={1} />
                </div>
            </Background>




            {/* <Container className={classes.containerPicture} maxWidth='xl'>
                    <div className={classes.containerPictureHue}></div>
                    {windowSize ? (<div className={classes.menu}>
                        <div className={classes.logoIcon}>
                            <img src={LogoIcon} alt="" />
                        </div>
                        <div className={classes.menuContent}>
                            <Link href="#" className={classes.menuItems}>Home</Link>
                            <Link href="#" className={classes.menuItems}>Services</Link>
                            <Link href="#" className={classes.menuItems}>Compliance</Link>
                            <Link href="#" className={classes.menuItems}>About Us</Link>
                        </div>
                    </div>) : (<div></div>)}
                    <div className={classes.mainContent}>
                        <Typography className={classes.mainContentText}>
                            We do all the work. You get the Credit!
                        </Typography>
                        <TextButton text={'Start Return'} version={1} />
                    </div>

                </Container> */}

        </div>
    );
}