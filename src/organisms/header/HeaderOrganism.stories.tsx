import { ComponentStory } from '@storybook/react';
import { HeaderOrganism } from './HeaderOrganism';
export default {
    title: 'Organisms/Header ',
    component: HeaderOrganism,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof HeaderOrganism> = args =>  <HeaderOrganism {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    
}