import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { default as Banner } from "../../assets/banner.png";
import { HeaderOrganismProps } from './HeaderOrganism';

export interface HeaderOrganismStylesProps {
    
}
export const useStyles = makeStyles<Theme, HeaderOrganismProps>((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            height: 65,
            paddingBottom: 766,
            zIndex: 1
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            display: 'flex',
            alignItems: 'center',
            color: '#FFFFFF',
            fontSize: 19,
            textTransform: 'capitalize',
            borderRadius: 0
        },
        headphoneIcon: {
            paddingTop: 7
        },
        navbar: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',

        },
        navbarContent: {
            display: 'flex',
            justifyContent: 'space-between',
            width: 340,

        },
        navbarMain: {
            paddingLeft: 24,
            color: '#ffffff',
            [theme.breakpoints.down('xs')]: {
                paddingLeft: 0
            }
        },
        loginButton: {
            background: 'transparent linear-gradient(107deg, #A61C14 0%, #530E0A 100%) 0% 0% no-repeat padding-box',
            height: 65,
            borderRadius: 0,
            width: 188,
            [theme.breakpoints.down('xs')]: {

            }
        },
        containerPicture: {
            background: `transparent url(${Banner}) 0% 0% no-repeat padding-box`,
            height: 866,
            position: 'relative'
        },
        containerPictureHue: {
            background: '#211D57 0% 0% no-repeat padding-box',
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            mixBlendMode: 'multiply',
            opacity: 0.5
        },
        menu: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: 100,
        },
        mainContent: {
            width: '100%',
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column'
        },
        mainContentText: {
            fontSize: 65,
            fontWeight: 'bold',
            color: '#ffffff',
            textShadow: '0px 3px 6px #000000DE',
            zIndex: 1,
            [theme.breakpoints.down('xs')]: {
                fontSize: 30,
                textAlign: 'center'
            }
        },
        mainContentButton: {
            border: '3px solid #FFFFFF',
            borderRadius: '15px',
            width: 400,
            height: 91,
            color: '#ffffff',
            fontSize: 32,
            fontWeight: 'bold',
            textTransform: 'capitalize',
            [theme.breakpoints.down('xs')]: {
                marginTop: theme.spacing(3),
                width: 300,
                height: 70
            }
        },
        menuContent: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        menuItems: {
            color: '#ffffff',
            textTransform: 'capitalize',
            fontSize: 26,
            padding: theme.spacing(2),
            zIndex: 1,
            '&:active': {
                borderBottom: '3px sold blue',
                borderRadius: 3,
                '&::after': {
                    borderBottom: '3px sold blue',
                    borderRadius: 3,
                }
            },

        },
        logoIcon: {
            zIndex: 1
        },
        content: {
            zIndex: 1
        }
    
    }),
    
);