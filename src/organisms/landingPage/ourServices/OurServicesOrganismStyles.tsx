import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { OurServicesOrganismProps } from './OurServicesOrganism';

export interface OurServicesOrganismStylesProps {
    
}
export const useStyles = makeStyles<Theme, OurServicesOrganismProps>((theme: Theme) =>
    createStyles({
        title: {
            color: '#403F3F',
            fontSize: 42,
            fontWeight: 'bold',
            paddingBottom: theme.spacing(6)
        },      
        container: (props: OurServicesOrganismProps) => ({
            backgroundColor: props.backgroundColor,
            
            
            [theme.breakpoints.down('xs')]: {
                padding: theme.spacing(1),
                paddingLeft: theme.spacing(3),
                paddingRight: theme.spacing(4),
                paddingTop: theme.spacing(2)
            }
        }),
        image: {
            [theme.breakpoints.down('xs')]: {
                height: 300,
                width: 300
            }
        },
        readmoreRed: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            [theme.breakpoints.down('xs')]: {
                
            }
        },
        subtitleRed: {
            fontSize: 28,
            color: '#A61C14',
            fontWeight: 'bold',
            [theme.breakpoints.down('xs')]: {
                
            }
        },
        arrow: {
            paddingLeft: theme.spacing(2),
            [theme.breakpoints.down('xs')]: {
                padding: theme.spacing(3),
            }
        }
    }),
);