import { Box, Container, Grid, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { useStyles } from './OurServicesOrganismStyles';
import ArrowRightIconRed from '../../../static/Landing page/assets/right-arrow-red.svg';
import { FourTextPapers } from '../../../molecules/Four Text Papers/FourTextPapers';
import { TextBoxProps } from '../../../atoms/Text Box/TextBox';


export interface OurServicesOrganismProps {
    backgroundColor?: string
    title?: string,
    image?: string,
    textBoxes: Array<TextBoxProps> 
}

export const OurServicesOrganism: React.FC<OurServicesOrganismProps> = (props: OurServicesOrganismProps) => {

    const { image, title, backgroundColor, textBoxes } = props

    const classes = useStyles(props)

    const [windowSize, setWindowSize] = useState<boolean>(window.screen.availWidth > 500);
    useEffect(() => {
        const handleWindowResize = () => setWindowSize(window.innerWidth > 500 ? true : false);
        window.addEventListener("resize", handleWindowResize);


        return () => window.removeEventListener("resize", handleWindowResize);
    }, []);
    return (
        <Container className={classes.containerWhite} maxWidth='xl'>
            <Box display="flex" justifyContent="center">
                <Typography className={classes.title}>
                    {title}
                </Typography>
            </Box>
            <Grid spacing={0} container justifyContent="center">
                {windowSize ? <Grid item sm={6}>
                    <img className={classes.image} src={image} alt="" />
                </Grid> : <div></div>}

                <FourTextPapers textBoxes={textBoxes} />
                <Grid item sm={6}></Grid>
                <Grid item sm={6} xs={12} className={classes.readmoreRed}>
                    <Typography className={classes.subtitleRed}>
                        Read More
                    </Typography>
                    <img className={classes.arrow} src={ArrowRightIconRed} alt="" />
                </Grid>
                
            </Grid>
        </Container>
    );
}