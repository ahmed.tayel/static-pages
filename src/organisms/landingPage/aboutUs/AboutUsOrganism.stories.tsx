import { ComponentStory } from '@storybook/react';
import { AboutUsOrganism } from './AboutUsOrganism';
import { default as Group300 } from "../../../static/Landing page/assets/Group 300.png";

export default {
    title: 'Organisms/Landing Page/About Us',
    component: AboutUsOrganism,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof AboutUsOrganism> = args =>  <AboutUsOrganism {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    image: Group300,
    paragraphs: ['Rx Reverse Distributors, Inc. (RxRD) is an industry leader in pharmaceutical returns processing and disposal. With a reputation for exceptional personalized service and communication, the professionals at RxRD specialize in servicing independently owned pharmacies and small to medium-sized pharmacy chains across the United States.'],
    title: 'About Us'
}