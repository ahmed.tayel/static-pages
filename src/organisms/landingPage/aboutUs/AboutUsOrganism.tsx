import { Box, Container, Grid, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { TextBox } from '../../../atoms/Text Box/TextBox';
import { useStyles } from './AboutUsOrganismStyles';
import ArrowRightIconBlue from '../../../static/Landing page/assets/right-arrow-blue.svg';
import { default as Group300 } from "../../../static/Landing page/assets/Group 300.png";

export interface AboutUsOrganismProps {
    backgroundColor?: '#FBFBFB' | 'white',
    title?: string,
    image?: string,
    paragraphs: Array<string>
}

export const AboutUsOrganism: React.FC<AboutUsOrganismProps> = (props: AboutUsOrganismProps) => {

    const { title='About Us', image=Group300, paragraphs, backgroundColor='#FBFBFB'  } = props

    const classes = useStyles(props)

    const [windowSize, setWindowSize] = useState<boolean>(window.screen.availWidth > 500);
    useEffect(() => {
        const handleWindowResize = () => setWindowSize(window.innerWidth > 500 ? true : false);
        window.addEventListener("resize", handleWindowResize);


        return () => window.removeEventListener("resize", handleWindowResize);
    }, []);

    return (
        <Container className={classes.containerGrey} maxWidth='xl'>
            <Grid container  justifyContent={'flex-start'}>
                <Grid item sm={6} xs={12}>
                    <TextBox
                        bodyFontSize={18}
                        headerFontSize={40}
                        image={''}
                        paragraphs={paragraphs} title={title} titleColor={'#403F3F'} />
                    <Box className={classes.readmoreBlue}>
                        <Typography className={classes.subtitleBlue}>
                            Read More
                            <img className={classes.arrowBlue} src={ArrowRightIconBlue} alt="" />
                        </Typography>

                    </Box>
                </Grid>
                {windowSize ? (<Grid item sm={6} xs={12}>
                    <img src={image} alt="" />
                </Grid>) : <div></div>}
            </Grid>
        </Container>

    );
}