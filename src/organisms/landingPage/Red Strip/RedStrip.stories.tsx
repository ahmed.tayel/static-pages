import { ComponentStory } from '@storybook/react';
import { RedStrip } from './RedStrip';
export default {
    title: 'Organisms/Landing Page/Strip',
    component: RedStrip,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof RedStrip> = args =>  <RedStrip {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    text: 'If you would like one of our sales representatives to call you.',
    buttonText: 'Call Us'
}