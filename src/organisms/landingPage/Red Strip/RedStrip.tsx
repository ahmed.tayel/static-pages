import { Button, Container, Typography } from '@material-ui/core';
import React from 'react'
import { TextButton } from '../../../atoms/TextButton/TextButton';
import { useStyles } from './RedStripStyles';

export interface RedStripProps {
    text: string,
    buttonText: string
}

export const RedStrip: React.FC<RedStripProps> = (props: RedStripProps) => {

    const { text, buttonText } = props

    const classes = useStyles(props)

    return (
        <Container className={classes.redStrip} maxWidth='xl'>
            <Typography className={classes.stripText}>
                {text}
            </Typography>
            <TextButton text={buttonText} version={1} />
        </Container>
    );
}