import './App.css';
import { OurServices } from './static/Our Services/OurServices';
import { Header } from './components/Header';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Footer } from './components/Footer';
import { AboutUs } from './static/About us/AboutUs';
import { Compliance } from './static/Compliance/Compliance';
import { Faq } from './static/FAQ/Faq';
import { ContactUs } from './static/Contact us/ContactUs';
import { WhyChooseUs } from './static/Why Choose Us/WhyChooseUs';
import { LandingPage } from './static/Landing page/LandingPage';
import LandingPageTable from './static/Landing page/LandingPageTable';


function App() {
  return (
    
    <div className="App">
      <Router>
      <div>
        <Header />
        <Switch>
          
          <Route path="/ourservices">
            <OurServices />
          </Route>

          <Route path="/aboutus">
            <AboutUs />
          </Route>
          
          <Route path="/compliance">
            <Compliance />
          </Route>

          <Route path="/faq">
            <Faq />
          </Route>
          <Route path="/contactus">
            <ContactUs />
          </Route>
          <Route path="/whychooseus">
            <WhyChooseUs />
          </Route>
          <Route path="/landingpage">
            <LandingPage/>
          </Route>
      
      
        </Switch>
        <Footer  />
      </div>
    </Router>
    </div>
  );
}

export default App;
