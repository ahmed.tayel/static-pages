import { ComponentStory } from '@storybook/react';
import { CarouselMolecule } from './CarouselMolecule';
import { default as CustomerIcon } from "../../static/Landing page/assets/customer.png";

export default {
    title: 'Molecules/Carousel',
    component: CarouselMolecule,
    argTypes: {

    }
}

const Template: ComponentStory<typeof CarouselMolecule> = args => <CarouselMolecule {...args} />

export const PrimaryCarousel = Template.bind({})
PrimaryCarousel.args = {
    cards: [
        {
            title: 'First card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor: '#A61C14'
        },
        {
            title: 'Second card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor: '#A61C14'
        },
        {
            title: 'Third card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor: '#A61C14'
        },
        {
            title: 'Fourth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor: '#A61C14'
        },
        {
            title: 'Fifth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor: '#A61C14'
        }, {
            title: 'Sixth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: '',
            titleColor: '#A61C14'
        }
    ],
    version: 1
}

export const AlternateCarousel = Template.bind({})
AlternateCarousel.args = {
    cards: [
        {
            title: 'First card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor: '#A61C14'
        },
        {
            title: 'Second card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor: '#A61C14'
        },
        {
            title: 'Third card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor: '#A61C14'
        },
        {
            title: 'Fourth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor: '#A61C14'
        },
        {
            title: 'Fifth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor: '#A61C14'
        }, {
            title: 'Sixth card',
            paragraphs: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id pharetra nisl. Vivamus vulputate.'],
            image: CustomerIcon,
            titleColor: '#A61C14'
        }
    ],
    version: 2
}