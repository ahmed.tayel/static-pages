import React from 'react'
import Slider from 'react-slick';
import { CardMolecule, CardProps } from '../Card/CardMolecule';
import { useStyles } from './CarouselStyles';
import { default as NextArrow } from "../../static/Landing page/assets/arrow-next-carousel.svg";
import { default as PrevArrow } from "../../static/Landing page/assets/arrow-prev-carousel.svg";

import { Container } from '@material-ui/core';
import { AlternateCard } from '../AlterenateCard/AlternateCard';



export interface CarouselProps {
    cards: Array<CardProps>,
    version: 1 | 2 | undefined
}

export const CarouselMolecule: React.FC<CarouselProps> = (props: CarouselProps) => {
    const settingCardCarousel = {
        arrows: true,
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 2,
        initialSlide: 2,
        nextArrow: <img src={NextArrow} />,
        prevArrow: <img src={PrevArrow} />,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };
    const settingCustomerCarousel = {
        arrows: true,
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        nextArrow: <img src={NextArrow} />,
        prevArrow: <img src={PrevArrow} />,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }
        ]
    };
    const { cards, version } = props

    const classes = useStyles(props)
    const loadCards = () => {

        if (version === 1) {
            return (cards.map((card) => {
                return <CardMolecule titleColor={'#403F3F'} image={card.image} paragraphs={card.paragraphs} title={card.title} key={card.title} />
            })
            )
        } else if ( version === 2 ) {
            return (cards.map((card) => {
                return <AlternateCard image={card.image} quote={card.paragraphs[0]} title={card.title} key={card.title} />
            })
            )
        }
    }

    return (
        
            <Slider { ...version === 1 ? {...settingCardCarousel} : {...settingCustomerCarousel}}>
                {loadCards()}
            </Slider>
        
    );
}


