import { ComponentStory } from "@storybook/react";
import { HeaderMolecule } from "./HeaderMolecule";

export default {
    title: 'Molecules/Navbar',
    component: HeaderMolecule,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof HeaderMolecule> = args =>  <HeaderMolecule {...args}/>

export const Primary = Template.bind({})
Primary.args = {
    
}