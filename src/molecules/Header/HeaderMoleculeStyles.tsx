import { createStyles, makeStyles, Theme } from "@material-ui/core";
import { BaseCSSProperties } from "@material-ui/core/styles/withStyles";
import { HeaderMoleculeProps } from "./HeaderMolecule";

export interface StyleProps {
    navbar: BaseCSSProperties,
    menu:BaseCSSProperties
}

export const useStyles = makeStyles<Theme, HeaderMoleculeProps>((theme: Theme) =>
    createStyles({
        navbar: (props: HeaderMoleculeProps) => ({
            width: 'auto',
            height: '65px',
            backgroundColor: "#211D57",
            display: 'flex',
            justifyContent: 'space-between',
            alignContent: 'center',
        }),
        menu: (props: HeaderMoleculeProps) => ({
            display: 'flex',
            width: 300,
            justifyContent: 'flex-end',
            [theme.breakpoints.down('xs')]: {
                width:85
            }
        })
    }),
);