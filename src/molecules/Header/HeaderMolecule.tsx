import React, { useEffect, useState } from 'react'
import { HeaderButton } from '../../atoms/Header Button/HeaderButton';
import { useStyles } from './HeaderMoleculeStyles';
import { default as Headphone } from "../../assets/headphone.svg";
import { default as UserIcon } from '../../assets/userIcon.svg';
import { default as LoginIcon } from '../../assets/loginIcon.svg';
import { default as Burger } from '../../assets/menu.svg';
import { default as LogoText } from '../../assets/logoText.svg';
import { Icon } from '../../atoms/Icon Button/Icon';

export interface HeaderMoleculeProps {


}

export const HeaderMolecule: React.FC<HeaderMoleculeProps> = (props: HeaderMoleculeProps) => {
    const { } = props
    const classes = useStyles(props)
    const [windowSize, setWindowSize] = useState<boolean>(window.screen.availWidth > 500);
    useEffect(() => {
        const handleWindowResize = () => setWindowSize(window.innerWidth > 500 ? true : false);
        window.addEventListener("resize", handleWindowResize);


        return () => window.removeEventListener("resize", handleWindowResize);
    }, []);
    return windowSize ? (
        <div className={classes.navbar}>
            <HeaderButton fontSize="19px" label={windowSize ? 'Contact Us' : ''} textColor="#ffffff"
                background="" icon={Headphone} width="" />
            <div className={classes.menu}>
                <HeaderButton fontSize="21px" label="Register" textColor="#ffffff"
                    background="" icon={UserIcon} width="" />
                <HeaderButton fontSize="21px" label="Login" textColor="#ffffff"
                    background="transparent linear-gradient(107deg, #A61C14 0%, #530E0A 100%) 0% 0% no-repeat padding-box"
                    icon={LoginIcon} width="188px" />
            </div>
        </div>
    ) : (<div className={classes.navbar}>
        <Icon icon={Burger} />
        <div className={classes.menu}>
            <Icon icon={UserIcon} />
        </div>
    </div>);
}