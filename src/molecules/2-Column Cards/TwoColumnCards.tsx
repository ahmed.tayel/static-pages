import { Grid } from '@material-ui/core';
import React from 'react'
import { CardMolecule, CardProps } from '../Card/CardMolecule';

export interface TwoColumnCardsProps {
    cards: Array<CardProps>
}

export const TwoColumnCards: React.FC<TwoColumnCardsProps> = (props: TwoColumnCardsProps) => {
    const { cards } = props

    const loadCards = () => {
        return (cards.map((card) => {
            return (
                <Grid item sm={6} xs={12}>
                    <CardMolecule titleColor={card.titleColor} image={card.image} paragraphs={card.paragraphs} title={card.title} />
                </Grid>
            )
        }))
    }

    return (
        <Grid container spacing={4}>
            {loadCards()}
        </Grid>
    );
}