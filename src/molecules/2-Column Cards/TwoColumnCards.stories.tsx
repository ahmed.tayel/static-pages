import { ComponentStory } from '@storybook/react';
import { TwoColumnCards } from './TwoColumnCards';
import { default as Minimium } from "../../static/Why Choose Us/assets/Minimum.svg";
import { default as RapidTurnaround } from "../../static/Why Choose Us/assets/RapidTurnaround.svg";

export default {
    title: 'Molecules/Two Column Cards',
    component: TwoColumnCards,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof TwoColumnCards> = args =>  <TwoColumnCards {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    cards: [
        {
            image: Minimium,
            paragraphs: ['Unlike other return services in the industry, RxRD is exceedingly persistent in identifying and recovering the maximum amount of cash credit for your expired and unwanted pharmaceuticals. RxRD makes the process simple with no product exclusions, no invoices, no out-of-pocket expenses, and no contract terms.'],
            title: 'Maximum Return, Minimum Hassle',
            titleColor: '#403F3F'
        },
        {
            image: RapidTurnaround,
            paragraphs: ["Through leading-edge tracking technology and efficient internal processing practices, RxRD typically processes pharmaceutical returns quicker than the industry average. RxRD has long-standing partnerships with the Nation's leading manufacturers, wholesalers, and buying groups. The RxRD accounting team will follow-up with ALL of them until you get the full the credit possible for returned products."],
            title: 'Rapid Turnaround',
            titleColor: '#A61C14'
        }
    ]
}