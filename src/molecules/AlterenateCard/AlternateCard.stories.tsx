import { ComponentStory } from '@storybook/react';
import { AlternateCard } from './AlternateCard';
import { default as CustomerIcon } from "../../static/Landing page/assets/customer.png";
export default {
    title: 'Molecules/AlternateCard',
    component: AlternateCard,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof AlternateCard> = args =>  <AlternateCard {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    image: CustomerIcon,
    title: "Jana B. - The Medicine Shoppe",
    quote: "They have streamlined the returns process to the point that what used to be a big hassle, is now a very simple process that takes very little time on our part."
    
}