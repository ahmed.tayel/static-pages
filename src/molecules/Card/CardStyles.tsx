
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { CardProps } from './CardMolecule';

export interface CardStylesStyleProps {
    cardCarousel: BaseCSSProperties,
    title: BaseCSSProperties,
    paragraph: BaseCSSProperties
}
export const useStyles = makeStyles<Theme, CardProps>((theme: Theme) =>
    createStyles({
        cardCarousel: {
            textAlign: 'left',
            backgroundColor: '#FFFFFF',
            padding: theme.spacing(1),
            
        },
        title: {
            fontSize: 24,
            color: '#403F3F',
            fontWeight: 'bold',
            marginBottom: 18
        },
        paragraph: {
            fontSize: 18,
            color: '#707070',
        },
    }),
);