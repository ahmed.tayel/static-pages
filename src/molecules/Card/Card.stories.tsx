import { ComponentStory } from '@storybook/react';
import { CardMolecule } from './CardMolecule';
import { default as SelfService } from "../../static/Landing page/assets/SelfService.svg";

export default {
    title: 'Molecules/Card',
    component: CardMolecule,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof CardMolecule> = args =>  <CardMolecule {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    title: "Title",
    paragraphs: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin interdum dui. Proin posuere libero dui, ac semper enim consequat a. Fusce vulputate venenatis augue. Etiam sed nulla ut nibh bibendum semper ac non arcu. In faucibus pellentesque ipsum, sit amet facilisis ipsum eleifend vitae. Etiam id scelerisque purus. Praesent id dui nec sem facilisis blandit nec ac leo. Curabitur ac augue et libero convallis semper ut eget metus. Maecenas porta eget est non sollicitudin. Ut porttitor, risus eu imperdiet malesuada, mi magna eleifend dui, ac vestibulum arcu diam ac nisl. Proin blandit a nunc et consequat."],
    image: '',
    titleColor: '#A61C14'
}

export const CardWithPicture = Template.bind({})
CardWithPicture.args = {
    title: "Title",
    paragraphs:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin interdum dui. Proin posuere libero dui, ac semper enim consequat a. Fusce vulputate venenatis augue. Etiam sed nulla ut nibh bibendum semper ac non arcu. In faucibus pellentesque ipsum, sit amet facilisis ipsum eleifend vitae. Etiam id scelerisque purus. Praesent id dui nec sem facilisis blandit nec ac leo. Curabitur ac augue et libero convallis semper ut eget metus. Maecenas porta eget est non sollicitudin. Ut porttitor, risus eu imperdiet malesuada, mi magna eleifend dui, ac vestibulum arcu diam ac nisl. Proin blandit a nunc et consequat."],
    image: SelfService,
    titleColor: '#403F3F'
}