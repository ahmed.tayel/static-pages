import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

export interface TableRowStylesProps {

}
export const useStyles = makeStyles<Theme, TableRowStylesProps>((theme: Theme) =>
    createStyles({
        tableRow: {
            '&:nth-of-type(odd)': {
                backgroundColor: theme.palette.action.hover,
            }
        }
    }),
);