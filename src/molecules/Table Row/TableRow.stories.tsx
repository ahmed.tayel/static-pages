import { ComponentStory } from '@storybook/react';
import { TableRowAtom } from './TableRowAtom';
export default {
    title: 'Molecules/Row',
    component: TableRowAtom,
    argTypes: {

    }
}

const Template: ComponentStory<typeof TableRowAtom> = args => <TableRowAtom {...args} />

export const Primary = Template.bind({})
Primary.args = {
    cells: [
        {
            clickable: false,
            header: false,
            onClick: undefined,
            text: "I am a normal cell.",
            bold: false
        },
        {
            clickable: false,
            header: false,
            onClick: undefined,
            text: "I am a normal cell but bold.",
            bold: true
        },
        {
            clickable: true,
            header: false,
            onClick: undefined,
            text: "I am a clickable cell.",
            bold: false
        },
        {
            clickable: true,
            header: true,
            onClick: undefined,
            text: "I am a clickable header cell.",
            bold: false
        },
        {
            clickable: false,
            header: true,
            onClick: undefined,
            text: "I am a header cell.",
            bold: false
        },
        {
            clickable: true,
            header: false,
            onClick: () => { alert("Hello!") },
            text: "I have an onclick event!",
            bold: false
        }
    ]
}