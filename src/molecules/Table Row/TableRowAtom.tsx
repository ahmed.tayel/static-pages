import { Paper, TableBody, TableHead } from '@material-ui/core';
import { Table, TableCell, TableContainer, TableRow } from '@material-ui/core';
import React from 'react'
import { TableCellAtom, TableCellAtomProps } from '../../atoms/Table Cell/TableCellAtom';

import { AccountManagerRow, ChecksRow, PharmacyRow, ReportsRow, ReturnRow } from './RowInterfaces';
import { useStyles } from './TableRowStyles';



export interface TableRowProps {
    cells: Array<TableCellAtomProps>,


}

export const TableRowAtom: React.FC<TableRowProps> = (props: TableRowProps) => {
    const { cells } = props

    const classes = useStyles(props)

    const loadCells = () => {
        return (cells.map((cell) => {
            return (
                <TableCellAtom bold={cell.bold} clickable={cell.clickable} header={cell.header} onClick={cell.onClick} text={cell.text} />
            )
        }))
    }
    return (
        <TableRow className={classes.tableRow}>
            {loadCells()}
        </TableRow>

    );
}