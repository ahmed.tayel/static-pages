export interface AccountManagerRow {
    account: string,
    returns: string,
    checks: string,
    startReturn: string
}

export interface ChecksRow {
    statment: string,
    paymentDate: string,
    invoiceDate: string,
    checkAmount: string,
    amount: string,
    checkStatus: string,
    returnNumber: string, 
    checkNumber: string
}
export interface ReturnRow {
    number: string,
    type: string,
    userItems: string,
    verifiedItems: string,
    status: string,
    upsLabel: string,
    reports: string, 
    erv: string,
    wholesalerCredit: string,
    checks: string, 
    disbursments: string, 
    serviceFee: string,
    deleted: boolean
}

export interface PharmacyRow {
    NDC: string,
    manufacturer: string,
    product: string,
    packageSize: string,
    type: string,
    fullQuantity: string,
    partialQuantity: string, 
    expirationDate: string,
    lotNumber: string,
    image: string, 
}
export interface ReportsRow {
    returnNumber: string,
    invoiceDate: string,
    serviceDate: string,
    erv: string,
    creditRecieved: string,
    directPay: string,
    fees: string, 
    amountPaid: string,
    lastPaymentAmound: string,
    remaingErv: string, 
    futureDated: string
}