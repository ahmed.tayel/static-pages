import { ComponentStory } from '@storybook/react';
import { TextIcon } from './TextIcon';
import { default as BriefcaseIcon } from '../../assets/briefcase.svg'

export default {
    title: 'Molecules/TextIcon',
    component: TextIcon,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof TextIcon> = args =>  <TextIcon {...args}/>
    
export const Primary = Template.bind({})
Primary.args = {
    text: "Helloo",
    icon: BriefcaseIcon
    
}