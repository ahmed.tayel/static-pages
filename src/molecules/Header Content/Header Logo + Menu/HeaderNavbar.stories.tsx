import { ComponentStory } from '@storybook/react';
import { HeaderNavbar } from './HeaderNavbar';
export default {
    title: 'Molecules/HeaderNavbar',
    component: HeaderNavbar,
    argTypes: {
        
    }
}

const Template:ComponentStory<typeof HeaderNavbar> = args =>  <HeaderNavbar {...args}/>
    
export const Navbar = Template.bind({})
Navbar.args = {
    
}