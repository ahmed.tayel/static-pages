import React from 'react'
import { AlternateLogo } from '../../../atoms/Header Content/Alternate Logo/AlternateLogo';
import { HeaderMenu, MenuItem } from '../Header Menu/HeaderMenu';
import { useStyles } from './HeaderNavbarStyles';

export interface HeaderNavbarProps {

}

export const HeaderNavbar: React.FC<HeaderNavbarProps> = (props: HeaderNavbarProps) => {
    const { } = props


    const { menu, logoIcon } = useStyles(props)
    return (
        <div className={menu}>
            <AlternateLogo height="200px" width="200px" />
            <div className={logoIcon}>
                <HeaderMenu />
            </div>
        </div>
    );
}