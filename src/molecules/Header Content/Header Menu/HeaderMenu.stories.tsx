import { ComponentStory } from '@storybook/react';
import { HeaderMenu } from './HeaderMenu';

export default {
    title: 'Molecules/HeaderContentMenu',
    component: HeaderMenu,
    argTypes: {

    }
}

const Template: ComponentStory<typeof HeaderMenu> = args => <HeaderMenu {...args} />

export const LandingPage = Template.bind({})
LandingPage.args = {
    
}