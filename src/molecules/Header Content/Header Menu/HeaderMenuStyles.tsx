import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { HeaderMenuProps } from './HeaderMenu';

export interface HeaderContentStylesProps {
    root: BaseCSSProperties
}
export const useStyles = makeStyles<Theme, HeaderMenuProps>((theme: Theme) =>
    createStyles({
        menuContent: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
    }),
);