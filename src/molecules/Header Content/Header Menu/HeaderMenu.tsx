import React from 'react'
import { MenuItem } from '../../../atoms/Header Content/Menu Item/MenuItem';
import { useStyles } from './HeaderMenuStyles';

export interface MenuItem {
    id: number,
    text: string,
    underline: "none" | "always" | "hover" | undefined
}

export interface HeaderMenuProps {
    
}

export const HeaderMenu: React.FC<HeaderMenuProps> = (props: HeaderMenuProps) => {
    const items:Array<MenuItem> = [
        {
            id: 1, 
            text: 'Home',
            underline: 'none'
        },
        {
            id: 2, 
            text: 'Services',
            underline: 'none'
        },{
            id: 3, 
            text: 'Compliance',
            underline: 'none'
        }
        ,{
            id: 4, 
            text: 'About Us',
            underline: 'none'
        }
    ]
    console.log(items)
    const { menuContent } = useStyles(props)

    const renderMenuItems = () => {
        return (items.map((item) => {
            return <MenuItem key={item.id} text={item.text} underline={item.underline} />
        })
        )
    }
    return (
        <div className={menuContent}>
            {renderMenuItems()}
        </div>
    );
}