import { ComponentStory } from '@storybook/react';
import { TableOrganism } from './TableOrganism';
export default {
    title: 'Organisms/Table',
    component: TableOrganism,
    argTypes: {

    }
}

const Template: ComponentStory<typeof TableOrganism> = args => <TableOrganism {...args} />

export const Primary = Template.bind({})
Primary.args = {
    rows: [
        {
            cells: [
                {
                    bold: false,
                    clickable: false,
                    header: false,
                    onClick: () => { },
                    text: "I'm a mere row"
                },
                {
                    bold: false,
                    clickable: false,
                    header: false,
                    onClick: () => { },
                    text: "I'm a mere row"
                },
                {
                    bold: false,
                    clickable: false,
                    header: false,
                    onClick: () => { },
                    text: "I'm a mere row"
                },
                {
                    bold: false,
                    clickable: false,
                    header: false,
                    onClick: () => { },
                    text: "I'm a mere row"
                }
            ]
        },
        {
            cells: [
                {
                    bold: false,
                    clickable: false,
                    header: false,
                    onClick: () => { },
                    text: "j"
                },
                {
                    bold: false,
                    clickable: false,
                    header: false,
                    onClick: () => { },
                    text: "j"
                },
                {
                    bold: false,
                    clickable: false,
                    header: false,
                    onClick: () => { },
                    text: "j"
                },
                {
                    bold: false,
                    clickable: false,
                    header: false,
                    onClick: () => { },
                    text: "j"
                }
            ]
        }
    ],
    header: {
        cells: [
            {
                bold: false,
                clickable: false,
                header: true,
                onClick: () => { },
                text: "I'm a header cell"
            },
            {
                bold: false,
                clickable: false,
                header: true,
                onClick: () => { },
                text: "I'm a header cell"
            },
            {
                bold: false,
                clickable: false,
                header: true,
                onClick: () => { },
                text: "I'm a header cell"
            },
            {
                bold: false,
                clickable: false,
                header: true,
                onClick: () => { },
                text: "I'm a header cell"
            }
        ]
    }
}