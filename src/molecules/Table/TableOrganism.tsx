import { Paper, Table, TableBody, TableContainer, TableHead, TableRow } from '@material-ui/core';
import React from 'react'
import { TableRowAtom, TableRowProps } from '../Table Row/TableRowAtom';
import { useStyles } from './TableOrganismStyles';

export interface TableMoleculeProps {
header: TableRowProps,
    rows: Array<TableRowProps>
}

export const TableOrganism: React.FC<TableMoleculeProps> = (props: TableMoleculeProps) => {
    const { rows, header } = props

    const classes = useStyles(props)

    const loadHeaderRow = () => {
        return (
            <TableRowAtom cells={header.cells} />
        )
    }

    const loadRows = () => {
        return (rows.map((row) => {
            return (
                <TableRowAtom cells={row.cells} />
            )
        })
        )
    }

    return (
        <TableContainer component={Paper} elevation={0}>
            <Table className={classes.table}>
                <TableHead>
                        {loadHeaderRow()}
                </TableHead>
                <TableBody component={Paper}>
                    {loadRows()}
                </TableBody>
            </Table>
        </TableContainer>
    );
}