import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';
import { TableMoleculeProps } from './TableOrganism';

export interface TableMoleculeStylesProps {

}
export const useStyles = makeStyles<Theme, TableMoleculeProps>((theme: Theme) =>
    createStyles({
        table: {
            minWidth: 700,
        },
        
    }),
);