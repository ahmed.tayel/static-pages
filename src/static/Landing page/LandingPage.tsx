import { Box, Button, Card, CardContent, Container, createStyles, Grid, Link, makeStyles, Paper, Theme, Typography } from '@material-ui/core';
import React from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { default as NextArrow } from "./assets/arrow-next-carousel.svg";
import { default as PrevArrow } from "./assets/arrow-prev-carousel.svg";
import { default as CustomerIcon } from "./assets/customer.png";
import { default as ExpressService } from "./assets/ExpressService.svg";
import { default as FullService } from "./assets/FullService.svg";
import { default as Group300 } from "./assets/Group 300.png";
import { default as Minimum } from "./assets/Minimum.svg";
import ArrowRightIconBlue from './assets/right-arrow-blue.svg';
import ArrowRightIconRed from './assets/right-arrow-red.svg';
import { default as RxDisposal } from "./assets/RxDisposal.svg";
import { default as SelfService } from "./assets/SelfService.svg";

import "./slickstyle.css";

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        minWidth: 275,
        padding: theme.spacing(8),
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(6),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(2),
            paddingRight: theme.spacing(2)
        }
    },
    containerGrey: {
        backgroundColor: '#FBFBFB',
        padding: theme.spacing(8),
        paddingLeft: theme.spacing(27),
        paddingRight: theme.spacing(27),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(3),
            paddingLeft: theme.spacing(3),
            paddingRight: theme.spacing(4),
            paddingTop: theme.spacing(2)
        }
    },
    containerWhite: {
        backgroundColor: '##FFFFFF',
        padding: theme.spacing(8),
        paddingLeft: theme.spacing(27),
        paddingRight: theme.spacing(27),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(1),
            paddingLeft: theme.spacing(3),
            paddingRight: theme.spacing(4),
            paddingTop: theme.spacing(2)
        }
    },

    paragraph: {
        fontSize: 18,
        color: '#707070',

    },
    title: {
        color: '#403F3F',
        fontSize: 42,
        fontWeight: 'bold',
        paddingBottom: theme.spacing(6)
    },
    subtitle: {
        fontSize: 24,
        color: '#403F3F',
        fontWeight: 'bold',
        marginTop: 19,
        marginBottom: 18
    },
    subtitleRed: {
        fontSize: 28,
        color: '#A61C14',
        fontWeight: 'bold',

    },
    subtitleBlue: {
        fontSize: 28,
        color: '#s323A87',
        fontWeight: 'bold',

    },
    list: {
        color: '#403F3F',
        fontSize: 18,
    },
    pos: {
        marginBottom: 12,
    },
    paperWhite: {

        textAlign: 'left',
        color: theme.palette.text.secondary,
        backgroundColor: '#FFFFFF',
        paddingTop: theme.spacing(3)
    },
    paperGrey: {

        textAlign: 'left',
        color: theme.palette.text.secondary,
        backgroundColor: '#FBFBFB',
        paddingTop: theme.spacing(3)
    },
    image: {
        [theme.breakpoints.down('xs')]: {
            height: 300,
            width: 300
        }
    },
    readmoreRed: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        [theme.breakpoints.down('xs')]: {
            paddingBottom: theme.spacing(1),
            paddingTop: theme.spacing(1)
        }
    },
    readmoreBlue: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        color: '#323A87',
        [theme.breakpoints.down('xs')]: {
            paddingBottom: theme.spacing(1),
            paddingTop: theme.spacing(1)
        }
    },
    arrow: {
        paddingRight: theme.spacing(16),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(3),
        }
    },
    arrowBlue: {
        paddingRight: theme.spacing(65),
        [theme.breakpoints.down('xs')]: {
            paddingRight: theme.spacing(12),
        }
    },
    redStripe: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        background: 'transparent linear-gradient(90deg, #A61C14 0%, #530E0A 100%) 0% 0% no-repeat padding-box',
        height: 162,
        [theme.breakpoints.down('xs')]: {
            flexDirection: 'column',
            height: 200
        }
    },
    stripeText: {
        color: '#FFFFFF',
        fontSize: 32,
        [theme.breakpoints.down('xs')]: {
            fontSize: 22,
        }
    },
    callUsButton: {
        border: '2px solid white',
        borderRadius: 15,
        color: '#FFFFFF',
        height: 91,
        width: 399,
        fontSize: 32,
        fontWeight: 'bold',
        textTransform: 'capitalize',
        [theme.breakpoints.down('xs')]: {
            width: 200,
            fontSize: 24,
            height: 60
        }
    },
    cardCarousel: {
        textAlign: 'left',
        backgroundColor: '#FFFFFF',
        padding: theme.spacing(1),
        marginRight: theme.spacing(4)
    },
    cardCustomerCarousel: {
        textAlign: 'center',
        backgroundColor: '#FFFFFF',
        padding: theme.spacing(1),
        marginRight: theme.spacing(4)
    },
    cardCarouselSubtitle: {
        color: '#323A87',
        fontSize: 26,
        fontWeight: 'bold',
        marginTop: 12,
        marginBottom: 36,

    },
    cardCarouselParagraph: {
        color: '#8A8A8A',
        fontSize: 26,

    },
    
}));

export const LandingPage: React.FC = () => {
    const classes = useStyles();
    const settingCardCarousel = {
        arrows: true,
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 2,
        initialSlide: 0,
        nextArrow: <img src={NextArrow} />,
        prevArrow: <img src={PrevArrow} />,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };
    const settingCustomerCarousel = {
        arrows: true,
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        nextArrow: <img src={NextArrow} />,
        prevArrow: <img src={PrevArrow} />,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }
        ]
    };

    return (
        <div>
            <Container className={classes.containerGrey} maxWidth='xl'>
                <Slider {...settingCardCarousel}>

                    <div>
                        <Card elevation={3} className={classes.cardCarousel}>
                            <CardContent>
                                <img src={Minimum} alt="" />
                                <Typography className={classes.subtitle}>
                                    Maximum Return, Minimum Hassle
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Unlike other return services in the industry, RxRD is exceedingly persistent in identifying and recovering the maximum amount of cash credit for your expired
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={3} className={classes.cardCarousel}>
                            <CardContent>
                                <img src={Minimum} alt="" />
                                <Typography className={classes.subtitle}>
                                    Maximum Return, Minimum Hassle
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Unlike other return services in the industry, RxRD is exceedingly persistent in identifying and recovering the maximum amount of cash credit for your expired
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={3} className={classes.cardCarousel}>
                            <CardContent>
                                <img src={Minimum} alt="" />
                                <Typography className={classes.subtitle}>
                                    Maximum Return, Minimum Hassle
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Unlike other return services in the industry, RxRD is exceedingly persistent in identifying and recovering the maximum amount of cash credit for your expired
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={3} className={classes.cardCarousel}>
                            <CardContent>
                                <img src={Minimum} alt="" />
                                <Typography className={classes.subtitle}>
                                    Maximum Return, Minimum Hassle
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Unlike other return services in the industry, RxRD is exceedingly persistent in identifying and recovering the maximum amount of cash credit for your expired
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={3} className={classes.cardCarousel}>
                            <CardContent>
                                <img src={Minimum} alt="" />
                                <Typography className={classes.subtitle}>
                                    Maximum Return, Minimum Hassle
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Unlike other return services in the industry, RxRD is exceedingly persistent in identifying and recovering the maximum amount of cash credit for your expired
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={3} className={classes.cardCarousel}>
                            <CardContent>
                                <img src={Minimum} alt="" />
                                <Typography className={classes.subtitle}>
                                    Maximum Return, Minimum Hassle
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Unlike other return services in the industry, RxRD is exceedingly persistent in identifying and recovering the maximum amount of cash credit for your expired
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>


                </Slider>
            </Container>
            <Container className={classes.containerWhite} maxWidth='xl'>
                <Box display="flex" justifyContent="center">
                    <Typography className={classes.title}>
                        Our Services
                    </Typography>
                </Box>
                <Grid container justifyContent="center">
                    <Grid item sm={6}>
                        <img className={classes.image} src={Group300} alt="" />
                    </Grid>
                    <Grid spacing={3} container item sm={6}>
                        <Grid item sm={6}>
                            <Paper className={classes.paperWhite} elevation={0}>
                                <img src={SelfService} alt="" />
                                <Typography className={classes.subtitle}>
                                    Self Service
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Our most popular and efficient service, customers have access to RxRD.com, on which they scan and create a return inventory.
                                    Each step of the process is
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={6}>
                            <Paper className={classes.paperWhite} elevation={0}>
                                <img src={ExpressService} alt="" />
                                <Typography className={classes.subtitle}>
                                    Express Service
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    This on-site service is available for those pharmacies which only require assistance with the inventory and processing of Schedule II-V Controlled Substances.
                                    Once
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={6}>
                            <Paper className={classes.paperWhite} elevation={0}>
                                <img src={FullService} alt="" />
                                <Typography className={classes.subtitle}>
                                    Full Service
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Our most comprehensive service, a regional RxRD representative will be assigned to your pharmacy, providing complete on-site account support.
                                    These
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={6}>
                            <Paper className={classes.paperWhite} elevation={0}>
                                <img src={RxDisposal} alt="" />
                                <Typography className={classes.subtitle}>
                                    RxDisposal
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Our RxDisposal service provides you with the responsible, safe and secure destruction of your waste pharmaceuticals.
                                    Using our customer returns portal you can
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={6} className={classes.readmoreRed}>
                            <Typography className={classes.subtitleRed}>
                                Read More

                            </Typography>
                            <img className={classes.arrow} src={ArrowRightIconRed} alt="" />
                        </Grid>
                    </Grid>
                </Grid>
            </Container>
            <Container className={classes.containerGrey} maxWidth='xl'>
                <Grid container>
                    <Grid item sm={6} xs={12}>
                        <Paper className={classes.paperGrey} elevation={0}>
                            <Typography className={classes.title}>
                                About Us
                            </Typography>
                            <Typography className={classes.paragraph}>
                                Rx Reverse Distributors, Inc. (RxRD) is an industry leader in pharmaceutical returns processing and disposal.
                                With a reputation for exceptional personalized service and communication, the professionals at RxRD specialize in servicing independently owned pharmacies and small to medium-sized pharmacy chains across the United States.
                            </Typography>
                            <Box className={classes.readmoreBlue}>
                                <Typography className={classes.subtitleBlue}>
                                    Read More
                                </Typography>
                                <img className={classes.arrowBlue} src={ArrowRightIconBlue} alt="" />
                            </Box>
                        </Paper>
                    </Grid>
                    <Grid item sm={6} xs={12}>
                        <img className={classes.image} src={Group300} alt="" />
                    </Grid>
                </Grid>
            </Container>
            <Container className={classes.containerWhite} maxWidth='xl'>
                <Slider {...settingCustomerCarousel}>

                    <div>
                        <Card elevation={0} className={classes.cardCustomerCarousel}>
                            <CardContent>
                                <Box display='flex' justifyContent='center' alignItems='center' flexDirection='column'>
                                    <img src={CustomerIcon} alt="" />
                                    <Typography className={classes.cardCarouselSubtitle}>
                                        Jana B. - The Medicine Shoppe
                                    </Typography>
                                    <Typography className={classes.cardCarouselParagraph}>
                                        "They have streamlined the returns process to the point that what used to be a big hassle, is now a very simple process that takes very little time on our part."
                                    </Typography>
                                </Box>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={0} className={classes.cardCustomerCarousel}>
                            <CardContent>
                                <Box display='flex' justifyContent='center' alignItems='center' flexDirection='column'>
                                    <img src={CustomerIcon} alt="" />
                                    <Typography className={classes.cardCarouselSubtitle}>
                                        Jana B. - The Medicine Shoppe
                                    </Typography>
                                    <Typography className={classes.cardCarouselParagraph}>
                                        "They have streamlined the returns process to the point that what used to be a big hassle, is now a very simple process that takes very little time on our part."
                                    </Typography>
                                </Box>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={0} className={classes.cardCustomerCarousel}>
                            <CardContent>
                                <Box display='flex' justifyContent='center' alignItems='center' flexDirection='column'>
                                    <img src={CustomerIcon} alt="" />
                                    <Typography className={classes.cardCarouselSubtitle}>
                                        Jana B. - The Medicine Shoppe
                                    </Typography>
                                    <Typography className={classes.cardCarouselParagraph}>
                                        "They have streamlined the returns process to the point that what used to be a big hassle, is now a very simple process that takes very little time on our part."
                                    </Typography>
                                </Box>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={0} className={classes.cardCustomerCarousel}>
                            <CardContent>
                                <Box display='flex' justifyContent='center' alignItems='center' flexDirection='column'>
                                    <img src={CustomerIcon} alt="" />
                                    <Typography className={classes.cardCarouselSubtitle}>
                                        Jana B. - The Medicine Shoppe
                                    </Typography>
                                    <Typography className={classes.cardCarouselParagraph}>
                                        "They have streamlined the returns process to the point that what used to be a big hassle, is now a very simple process that takes very little time on our part."
                                    </Typography>
                                </Box>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={0} className={classes.cardCustomerCarousel}>
                            <CardContent>
                                <Box display='flex' justifyContent='center' alignItems='center' flexDirection='column'>
                                    <img src={CustomerIcon} alt="" />
                                    <Typography className={classes.cardCarouselSubtitle}>
                                        Jana B. - The Medicine Shoppe
                                    </Typography>
                                    <Typography className={classes.cardCarouselParagraph}>
                                        "They have streamlined the returns process to the point that what used to be a big hassle, is now a very simple process that takes very little time on our part."
                                    </Typography>
                                </Box>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card elevation={0} className={classes.cardCustomerCarousel}>
                            <CardContent>
                                <Box display='flex' justifyContent='center' alignItems='center' flexDirection='column'>
                                    <img src={CustomerIcon} alt="" />
                                    <Typography className={classes.cardCarouselSubtitle}>
                                        Jana B. - The Medicine Shoppe
                                    </Typography>
                                    <Typography className={classes.cardCarouselParagraph}>
                                        "They have streamlined the returns process to the point that what used to be a big hassle, is now a very simple process that takes very little time on our part."
                                    </Typography>
                                </Box>
                            </CardContent>
                        </Card>
                    </div>


                </Slider>
            </Container>
            <Container className={classes.redStripe} maxWidth='xl'>

                <Typography className={classes.stripeText}>
                    If you would like one of our sales representatives to call you.
                </Typography>
                <Button className={classes.callUsButton} variant='outlined'>
                    Call Us
                </Button>
            </Container>
        </div>
    );
}