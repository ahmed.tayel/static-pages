import { Accordion, AccordionSummary, Card, CardContent, Container, createStyles, makeStyles, Theme, Typography } from '@material-ui/core';
import React from 'react'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { AccordionDetails } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        minWidth: 275,
        padding: theme.spacing(8),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(4),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(2),
            paddingRight: theme.spacing(2)
        }
    },
    acccordianDetails: {
        fontSize: 18,
        color: '#403F3F',
        paddingTop: '10',
        paddingBottom: '10'

    },
    accordianSummary: {
        fontSize: 18,
        color: '#403F3F',
        fontWeight: 'bold',
        marginTop : 10,
        
    },
    subtitle: {
        fontSize: 32,
        color: '#A61C14',
        fontWeight: 'bold',
        marginTop: 19,
        marginBottom: 59
    },
    list: {
        color: '#403F3F',
        fontSize: 18,
    },
    pos: {
        marginBottom: 12,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
}));
export const Faq: React.FC = ( ) => {
    const classes = useStyles();
    return (
        <Container>
            <Card className={classes.root}>
                <CardContent>
                    <Typography className={classes.subtitle}>
                        Frequently Asked Questions
                    </Typography>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                What do I do if my labels didn't print?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                How can I print more labels?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                               How do I reschedule or cancel a FedEx pick up?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                4- Q. Can I submit the return without scheduling a pick up?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                5- Q. Is there a cost to print a check?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                6- Q. Does this check expire?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                7- Q. Does the check link expire once printed?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                8- Q. What email address will my checks come from?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                8- Q. What email address will my checks come from?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                10- Q. Are Deposit Services available for all checks?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                11- Q. How will I know if a Deposit Service is available?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                11- Q. How will I know if a Deposit Service is available?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion elevation={0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.accordianSummary}>
                                11- Q. How will I know if a Deposit Service is available?
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography className={classes.acccordianDetails}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae eum qui animi perspiciatis sint quo, doloribus aspernatur rem aliquam vitae natus, beatae repudiandae nemo ullam.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                </CardContent>
            </Card>
        </Container>
    );
}