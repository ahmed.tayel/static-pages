import { Card, CardContent, Container, createStyles, Grid, makeStyles, Paper, Theme, Typography } from '@material-ui/core';
import React from 'react';
import { default as ExpressService } from "./assets/ExpressService.svg";
import { default as FullService } from "./assets/FullService.svg";
import { default as RxDisposal } from "./assets/RxDisposal.svg";
import { default as SelfService } from "./assets/SelfService.svg";

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        minWidth: 275,
        padding: theme.spacing(8),
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(6),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(2),
            paddingRight: theme.spacing(2)
        }
    },
    paragraph: {
        fontSize: 18,
        color: '#707070',

    },
    subtitle: {
        fontSize: 24,
        color: '#403F3F',
        fontWeight: 'bold',
        marginTop: 19,
        marginBottom: 18
    },
    list: {
        color: '#403F3F',
        fontSize: 18,
    },
    pos: {
        marginBottom: 12,
    },
    paper: {
        paddingTop: theme.spacing(3),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
}));

export const OurServices: React.FC = ( ) => {

    const classes = useStyles();
    return (
        <Container>
            <Card className={classes.root} elevation={5}>
                <CardContent>
                    <Typography className={classes.paragraph} gutterBottom>
                        Rx Reverse Distributors understands the process of returning expired or unwanted pharmaceuticals can be overwhelming and time-consuming. With hundreds of manufacturers, each with different return policies that are constantly changing, the experts at RxRD can manage the entire process to ensure you get the most cash back for your outdated product. Choose from any of the established service levels below, or we can work with you to develop a process that meets the return needs of your pharmacy.
                    </Typography>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={6}>
                            <Paper elevation={0} className={classes.paper}>
                                <img src={SelfService} alt="y" />
                                <Typography className={classes.subtitle} gutterBottom>
                                    Self Service
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Our most popular and efficient service, customers have access to RxRD.com, on which they scan and create a return inventory.
                                    Each step of the process is guided step-by-step with simple instructions.
                                    This helpful online resource is accessible 24/7 to request DEA forms, download inventory reports, print shipping labels, schedule pickup, and track the return to RxRD.
                                </Typography>
                                <br />
                                <Typography className={classes.paragraph}>
                                    In addition to online access, a dedicated account specialist will be assigned to your account.
                                    Your account expert will available by phone or email Monday through Friday from 9am-5pm to assist with your returns or answer any questions.
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Paper elevation={0} className={classes.paper}>
                                <img src={ExpressService} alt="y" />
                                <Typography className={classes.subtitle} gutterBottom>
                                    Express Service
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    This on-site service is available for those pharmacies which only require assistance with the inventory and processing of Schedule II-V Controlled Substances.
                                    Once you pull expired or unwanted product from your shelves and place them in a box for shipment, a RxRD representative will schedule a visit to your pharmacy to process these items for return.
                                    As part of this service, you will be provided a full inventory of the Controlled products, as well as all required DEA 222 forms.
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6}> 
                            <Paper elevation={0} className={classes.paper}>
                                <img src={FullService} alt="y" />
                                <Typography className={classes.subtitle} gutterBottom>
                                    Full Service
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Our most comprehensive service, a regional RxRD representative will be assigned to your pharmacy, providing complete on-site account support.
                                    These highly-trained field representatives will help you manage your processing timeline and establish an on-going pharmaceuticals return program on your behalf.
                                </Typography>
                                <br />
                                <Typography className={classes.paragraph}>
                                    At each scheduled processing, our returns experts will pull expired or unwanted pharmaceuticals from your shelves, package and label each product for shipping, and complete all required agency forms.
                                    In addition, you will receive a full inventory report detailing the value of the products as well as your estimated return value.
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Paper elevation={0} className={classes.paper}>
                                <img src={RxDisposal} alt="y" />
                                <Typography className={classes.subtitle} gutterBottom>
                                    RxDisposal
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Our RxDisposal service provides you with the responsible, safe and secure destruction of your waste pharmaceuticals.
                                    Using our customer returns portal you can create your inventory, request a DEA Form 222, print shipping labels and submit payment.
                                    <p style={{ color: '#403F3F', fontWeight: 'bolder' }}>The RxDisposal service is suitable for:</p>
                                </Typography>
                                <br />
                                <Grid container>
                                    <Grid item xs={6}>
                                        <Typography className={classes.list}>
                                            <ul>
                                                <li>Pharmacies</li>
                                                <li>Manufacturers</li>
                                                <li>Wholesalers</li>
                                                <li>Laboratories</li>
                                                <li>Hospitals</li>
                                                <li>Clinics</li>
                                            </ul>
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography className={classes.list}>
                                            <ul>
                                                <li>Pharmacies</li>
                                                <li>Manufacturers</li>
                                                <li>Wholesalers</li>
                                                <li>Laboratories</li>
                                                <li>Hospitals</li>
                                                <li>Clinics</li>
                                            </ul>
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </Container>
    );
}