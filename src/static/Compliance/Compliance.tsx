import { Card, CardContent, Container, createStyles, Grid, makeStyles, Paper, Theme, Typography } from '@material-ui/core';
import React from 'react';
import { default as DeaLogo } from "./assets/dea_logo.png";
import { default as DotLogo } from "./assets/dot_logo.png";
import { default as EpaLogo } from "./assets/epa_logo.png";
import { default as FldbprLogo } from "./assets/fldbpr_logo.png";
import { default as NabpSeal } from "./assets/nabp_seal.png";

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        minWidth: 275,
        padding: theme.spacing(8),
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(6),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(2),
            paddingRight: theme.spacing(2)
        }
    },
    paragraph: {
        fontSize: 18,
        color: '#403F3F',

    },
    subtitle: {
        fontSize: 21,
        color: '#A61C14',
        fontWeight: 'bold',
        paddingTop: 19,
        paddingBottom: 34
    },
    list: {
        color: '#403F3F',
        fontSize: 18,
    },
    pos: {
        marginBottom: 12,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        backgroundColor: '#EEEEEE',
        display: 'flex',
        alignItems: 'center',
        [theme.breakpoints.down('xs')]: {
            justifyContent: 'center'
        }
    },
    paperText: {
        fontSize: 21,
        color: '#403F3F',
        paddingLeft: 55,
        [theme.breakpoints.down('xs')]: {
            paddingLeft: 0
        }
    },
    disclaimer: {
        color: '#A61C14',
        fontSize: 18,
        marginTop: 28
    }
}));


export const Compliance: React.FC = ( ) => {
    const classes = useStyles();
    const stateNames = ['alabama']
    const states = (stateNames: Array<string>) => {
        return (
            <Grid item xs={6} sm={4}>
                <Paper className={classes.paper} elevation={0} >
                    <Typography className={classes.paperText}>
                        Alabama
                    </Typography>
                </Paper>
            </Grid>
        );
    }

    return (
        <Container>
            <Card className={classes.root}>
                <CardContent>
                    <Typography className={classes.paragraph}>
                        Rx Reverse Distributors, Inc. is federally licensed by the DEA, EPA and DOT as well as the State of Florida and individual states as required.
                        Our facility is accredited by the National Association of Boards of Pharmacy (NABP).
                        As a Verified Accredited Wholesale Distributor (VAWD) we have undergone a critera compliance review, including a rigourous review of our operating policies and procedures,
                        licensure verification, facility survey and operations, background checks and screening.
                    </Typography>
                    <Typography className={classes.subtitle}>
                        Federal Licenses
                    </Typography>
                    <Grid spacing={10} container justifyContent="center" alignItems="center">


                        <Grid item xs={6} sm>
                            <img src={DeaLogo} alt="" />
                        </Grid>
                        <Grid item xs={6} sm>
                            <img src={EpaLogo} alt="" />
                        </Grid>
                        <Grid item xs={6} sm>
                            <img src={DotLogo} alt="" />
                        </Grid>

                        <Grid item xs={6} sm>
                            <img src={FldbprLogo} alt="" />
                        </Grid>
                        <Grid item xs={6} sm>
                            <img src={NabpSeal} alt="" />
                        </Grid>



                    </Grid>
                    <Typography className={classes.subtitle}>
                        State Licenses
                    </Typography>
                    <Grid container spacing={2}>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <Paper className={classes.paper} elevation={0} >
                                <Typography className={classes.paperText}>
                                    Alabama
                                </Typography>
                            </Paper>
                        </Grid>
                    </Grid>
                    <Typography className={classes.disclaimer}>
                        *Rx Reverse Distributors, Inc. provides service in this state.
                        The state board of pharmacy currently does not require a license for reverse distribution.
                    </Typography>
                </CardContent>
            </Card>
        </Container>
    );
}