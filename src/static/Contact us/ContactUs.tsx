import { Button, Card, CardContent, Container, createStyles, Divider, Grid, makeStyles, Paper, SvgIcon, TextField, Theme, Typography } from '@material-ui/core';
import React, { useState } from 'react'
import { default as GoogleMap } from "./assets/Capture.png";
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        minWidth: 275,
        padding: theme.spacing(8),
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(6),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(2),
            paddingRight: theme.spacing(2)
        }

    },
    info: {
        fontSize: 21,
        color: '#403F3F',
        marginLeft: 8
    },
    paragraph: {
        color: '#403F3F',
        fontSize: 28,
    },
    subtitle: {
        fontSize: 32,
        color: '#A61C14',
        fontWeight: 'bold',
        marginTop: 19,
        marginBottom: 18
    },
    list: {
        color: '#403F3F',
        fontSize: 18,
    },
    pos: {
        marginBottom: 12,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
        display: 'flex',
    },
    icon: {
        color: '#323A87'
    },
    button: {
        background: 'transparent linear-gradient(88deg, #A61C14 0%, #530E0A 100%) 0% 0% no-repeat padding-box',
        color: '#FFFFFF',
        fontSize: 32,
        fontWeight: 'bold',
        width: 1100,

        [theme.breakpoints.down('xs')]: {
            width: 300,
            marginLeft: 0
        }
    }
}));

export const ContactUs: React.FC = ( ) => {
    const classes = useStyles()

    const [name, setName] = useState<string>()
    const [company, setCompany] = useState<string>()
    const [email, setEmail] = useState<string>()
    const [phoneNumber, setPhoneNumber] = useState<string>()

    const handleSubmission = () => {

    }
    return (
        <Container >
            <Card className={classes.root}>
                <CardContent>
                    <img src={GoogleMap} alt="" />
                    <Grid container>
                        <Grid item sm={4}>
                            <Paper className={classes.paper} elevation={0}>
                                <SvgIcon component={BusinessCenterIcon} className={classes.icon} />
                                <Typography className={classes.info}>
                                    Rx REVERSE DISTRIBUTORS, INC. 9255 US Highway 1 Sebastian, FL 32958
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={4}>
                            <Paper className={classes.paper} elevation={0}>
                                <SvgIcon component={BusinessCenterIcon} className={classes.icon} />
                                <Typography className={classes.info}>
                                    866-388-7973
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={4}>
                        </Grid>
                        <Grid item sm={4}>
                            <Paper className={classes.paper} elevation={0}>
                                <SvgIcon component={PhoneIcon} className={classes.icon} />
                                <Typography className={classes.info}>
                                    866-388-7973
                                </Typography>
                            </Paper>
                        </Grid>
                        <Grid item sm={4}>
                            <Paper className={classes.paper} elevation={0}>
                                <SvgIcon component={EmailIcon} className={classes.icon} />
                                <Typography className={classes.info}>
                                    Contact Email
                                </Typography>
                            </Paper>
                        </Grid>
                    </Grid>
                    <Divider light />
                    <Typography className={classes.subtitle}>
                        Call Us
                    </Typography>
                    <Typography className={classes.paragraph}>
                        If you would like one of our sales representatives to call you. Simply fill out the form below and click "Call Me".
                    </Typography>
                    <Paper className={classes.paper} elevation={0}>

                        <Grid container spacing={4}>
                            <Grid item sm={6} xs={12}>
                                <TextField onBlur={(event) => setName(event.target.value)} fullWidth variant="outlined" label="Name" name="name" />
                            </Grid>
                            <Grid item sm={6} xs={12}>
                                <TextField onBlur={(event) => setCompany(event.target.value)} fullWidth variant="outlined" label="Company" name="company" />
                            </Grid>
                            <Grid item sm={6} xs={12}>
                                <TextField onBlur={(event) => setEmail(event.target.value)} fullWidth variant="outlined" label="Email" name="email" />
                            </Grid>
                            <Grid item sm={6} xs={12}>
                                <TextField onBlur={(event) => setPhoneNumber(event.target.value)} fullWidth variant="outlined" label="Phone Number" name="phoneNumber" />
                            </Grid>
                        </Grid>
                    </Paper>
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid item sm={12}>
                            <Button onClick={handleSubmission} disableElevation className={classes.button}>Call us</Button>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </Container >
    );
}