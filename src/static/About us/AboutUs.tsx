import { Card, CardContent, Container, createStyles, makeStyles, Theme, Typography } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        minWidth: 275,
        padding: theme.spacing(10),
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
        zIndex: 1,
        borderRadius: 25,
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(1)
        }
    },
    paragraph: {
        fontSize: 18,
        color: '#403F3F',

    },
    subtitle: {
        fontSize: 24,
        color: '#403F3F',
        fontWeight: 'bold',
        marginTop: 19,
        marginBottom: 18
    },
    list: {
        color: '#403F3F',
        fontSize: 18,
    },
    pos: {
        marginBottom: 12,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
}));

export const AboutUs: React.FC = () => {
    const classes = useStyles();
    return (
        <Container>
            <Card className={classes.root}>
                <CardContent>
                    <Typography className={classes.paragraph}>
                        Rx Reverse Distributors, Inc. (RxRD) is an industry leader in pharmaceutical returns processing and disposal.
                        With a reputation for exceptional personalized service and communication, the professionals at RxRD specialize in servicing independently owned pharmacies and
                        small to medium-sized pharmacy chains across the United States.
                    </Typography>
                    <br />
                    <Typography className={classes.paragraph}>
                        With the development and implementation of proprietary processing and tracking software, the company is a technological innovator in the industry.
                        The easy to use website created by RxRD gives customers real-time access to the returns process from initial inventory to reconciliation.
                    </Typography>
                    <br />
                    <Typography className={classes.paragraph}>
                        RxRD is fully committed to identifying and recovering all credits due to their customers.
                        In addition to the web portal, detailed communication is provided throughout the process via a dedicated support representative assigned to each account.
                    </Typography>
                    <br />
                    <Typography className={classes.paragraph}>
                        RxRD is federally licensed by the DEA, DOT, and EPA, as well as the State of Florida and individual states as required.
                        Each employee undergoes extensive background checks and training, ensuring full compliance with government agency regulations.
                    </Typography>
                    <br />
                    <Typography className={classes.paragraph}>
                        With a company culture focused on operating in accordance with all rules and regulations,
                        RxRD also provides customers with peace of mind knowing their expired products are being disposed of in an environmentally safe and effective manner.
                    </Typography>
                    <br />
                    <Typography className={classes.paragraph}>As a family-owned and operated company, RxRD also understands the importance of developing long-standing business relationships built on trust and integrity.
                        They believe in full transparency in communication and business practices.
                        RxRD customers know they are receiving the very best in service, consistently exceeding expectations.
                    </Typography>
                </CardContent>
            </Card>
        </Container>
    );
}