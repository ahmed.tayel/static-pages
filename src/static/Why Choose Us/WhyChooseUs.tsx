import { Card, CardContent, Container, createStyles, Grid, makeStyles, Theme, Typography } from '@material-ui/core';
import React from 'react';
import { default as CompReport } from "./assets/ComprehensiveReporting.svg";
import { default as Minimium } from "./assets/Minimum.svg";
import { default as RapidTurnaround } from "./assets/RapidTurnaround.svg";
import { default as UnparalleledSupport } from "./assets/UnparalleledSupport.svg";
import { default as Technology } from "./assets/Technology.svg";
import { default as Programs } from "./assets/PersonalizedPrograms.svg";

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        minWidth: 275,
        padding: theme.spacing(8),
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(6),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(2),
            paddingRight: theme.spacing(2)
        }
    },
    paragraph: {
        fontSize: 18,
        color: '#707070',
        marginBottom: theme.spacing(2)
    },
    subtitle: {
        fontSize: 24,
        color: '#403F3F',
        fontWeight: 'bold',
        marginTop: 19,
        marginBottom: 18
    },
    list: {
        color: '#403F3F',
        fontSize: 18,
    },
    pos: {
        marginBottom: 12,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    card: {
        padding: theme.spacing(3),
        minHeight: 420
    }
}));

export const WhyChooseUs: React.FC = ( ) => {
    const classes = useStyles();
    return (
        <Container>
            <Card className={classes.root}>
                <CardContent>
                    <Typography className={classes.paragraph}>
                        Many times, making a change in your pharmaceutical returns process could result in an increase of thousands of dollars in cash back to your pharmacy.
                    </Typography>
                    <Typography className={classes.paragraph}>
                        Rx Reverse Distributors has earned a reputation in the industry for maximizing return credit on all expiring or unwanted pharmaceuticals, while making the process surprisingly uncomplicated.
                    </Typography>
                    <br />
                    <Grid container spacing={4}>
                        <Grid item sm={6} xs={12}>
                            <Card elevation={3} className={classes.card}>
                                <img src={Minimium} alt="" />
                                <Typography className={classes.subtitle}>
                                    Maximum Return, Minimum Hassle
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Unlike other return services in the industry, RxRD is exceedingly persistent in identifying and recovering the maximum amount of cash credit for your expired and unwanted pharmaceuticals.
                                    RxRD makes the process simple with no product exclusions, no invoices, no out-of-pocket expenses, and no contract terms.
                                </Typography>
                            </Card>
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <Card elevation={3} className={classes.card}>
                                <img src={UnparalleledSupport} alt="" />
                                <Typography className={classes.subtitle}>
                                    Unparalleled Support
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Every RxRD customer is assigned a dedicated support representative who will regularly analyze your account to maximize your return value.
                                    These service professionals are available to answer questions, alert you of return timelines, and assist in the inventory and tracking process.
                                </Typography>
                            </Card>
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <Card elevation={3} className={classes.card}>
                                <img src={RapidTurnaround} alt="" />
                                <Typography className={classes.subtitle}>
                                    Rapid Turnaround
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    Through leading-edge tracking technology and efficient internal processing practices,
                                    RxRD typically processes pharmaceutical returns quicker than the industry average.
                                    RxRD has long-standing partnerships with the Nation's leading manufacturers, wholesalers, and buying groups.
                                    The RxRD accounting team will follow-up with ALL of them until you get the full the credit possible for returned products.
                                </Typography>
                            </Card>
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <Card elevation={3} className={classes.card}>
                                <img src={CompReport} alt="" />
                                <Typography className={classes.subtitle}>
                                    Comprehensive Reporting
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    All transactions with RxRD are transparent and uncomplicated. You will be provided with easy-to-read inventory reports detailing your estimated cash return amount once we receive your shipment.
                                    After our team works diligently to get the maximum amount of cash from your returns, you will also receive a monthly cash statement, eliminating confusing wholesaler reconciliation reports.
                                </Typography>
                            </Card>
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <Card elevation={3} className={classes.card}>
                                <img src={Technology} alt="" />
                                <Typography className={classes.subtitle}>
                                    User-Friendly Technology
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    As an innovator in pharmaceutical returns and inventory technology, RxRD develops software and online applications that are simple for anyone to use.
                                    For those customers using the online portal, the returns process is guided step-by-step.
                                    There, you can download forms and shipping labels, print reports, schedule pickup, and even track the progress of your return.
                                </Typography>
                            </Card>
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <Card elevation={3} className={classes.card}>
                                <img src={Programs} alt="" />
                                <Typography className={classes.subtitle}>
                                    Personalized Programs
                                </Typography>
                                <Typography className={classes.paragraph}>
                                    As a family-owned and customer-focused company, RxRD offers various levels of service depending on the needs of your pharmacy.
                                    With the convenience of in-house or on-site services, the specialists at RxRD can help you find the program that is the right fit for your operation and budget.
                                </Typography>
                            </Card>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </Container>
    );
}